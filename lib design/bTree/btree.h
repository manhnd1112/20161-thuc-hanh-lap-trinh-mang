#include<stdio.h>
#include<conio.h>
#include<stdlib.h>
#include<stdbool.h>
#include<string.h>


typedef struct TreeNode TreeNode;

//dinh nghia ham insCompare, searchCompare,delCompare, printElement
struct TreeNode
{
    bTreeElementtype element;
    TreeNode* left;
    TreeNode* right;
};

typedef TreeNode* TreeType;
////
int insCompare(bTreeElementtype e1, bTreeElementtype e2);
int searchCompare(bTreeElementtype e1,bTreeElementtype e2);
int delCompare(bTreeElementtype e1,bTreeElementtype e2);
void printElement(bTreeElementtype e);
///
void makeTree(TreeType *tree);
TreeType leftTree(TreeType root);
TreeType rightTree(TreeType root);
bool isLeaf(TreeNode* node);
bool isEmptyTree(TreeType root);
TreeNode* makeTreeNode(bTreeElementtype e);
int numberNodeTree(TreeType root);
int heightOfTree(TreeType root);
int numberOfLeaf(TreeType root);
int numberOfInternalNode(TreeType root);
TreeType creatFrom2(TreeType l,TreeType r,bTreeElementtype e);
TreeNode* addLeft(TreeType *root,bTreeElementtype e);
TreeNode* addRight(TreeType *root,bTreeElementtype e);
int numberOfRightChildren(TreeType root);
void bInsert(TreeType *root,bTreeElementtype e);
void inOrderPrint(TreeType root);
void preOrderPrint(TreeType root);
TreeNode *bSearch(TreeType root,bTreeElementtype e);
TreeNode *findMin(TreeType root);
TreeNode *findMax(TreeType root);
void deleteNode(TreeType* root,bTreeElementtype e);
void deleteTree(TreeType* root);


void makeTree(TreeType *tree)
{
    (*tree) = NULL;
}

TreeType leftTree(TreeType root)
{
    if(root!=NULL)
        return root->left;
}

TreeType rightTree(TreeType root)
{
    if(root!=NULL)
        return root->right;
}

bool isLeaf(TreeNode* node)
{
    if(node!=NULL)
        return node->left == NULL && node->right == NULL;
    else return false;
}

bool isEmptyTree(TreeType root)
{
    return root == NULL;
}

TreeNode* makeTreeNode(bTreeElementtype e)
{
    TreeNode *node;
    node = (TreeNode *)malloc(sizeof(TreeNode));
    if(node!= NULL)
    {
        node->element = e;
        node->left = NULL;
        node->right = NULL;
    }
    return node;
}

int numberNodeTree(TreeType root)
{
    if(root == NULL) return 0;
    return 1 + numberNodeTree(leftTree(root)) + numberNodeTree(rightTree(root));
}


int heightOfTree(TreeType root)
{
    if(root == NULL) return 0;
    int n1,n2;
    n1 = heightOfTree(leftTree(root));
    n2 = heightOfTree(rightTree(root));
    if(n1>=n2) return 1 + n1;
    else return 1 + n2;
}

int numberOfLeaf(TreeType root)
{
    if(root == NULL) return 0;
    if(isLeaf(root)) return 1;
    return numberOfLeaf(leftTree(root)) + numberOfLeaf(rightTree(root));
}

int numberOfInternalNode(TreeType root)
{
    if(root == NULL || isLeaf(root) ) return 0;
    return 1 + numberOfInternalNode(leftTree(root)) + numberOfInternalNode(rightTree(root));
}

TreeType creatFrom2(TreeType l,TreeType r,bTreeElementtype e)
{
    TreeType root;
    root = (TreeNode*)malloc(sizeof(TreeNode));
    if(root!=NULL)
    {
        root->element = e;
        root->left = l;
        root->right = r;
    }
    return root;
}

// add node vào trái nhất, trả về nốt được add
TreeNode* addLeft(TreeType *root,bTreeElementtype e)
{
    TreeNode* node = makeTreeNode(e);
    if(node==NULL) return NULL;
    if(*root==NULL) *root = node;
    else
    {
        TreeNode* LNode = *root;
        while(LNode->left != NULL)
        LNode = LNode->left;
        LNode->left = node;
    }
    return node;
}

// add node vào phải nhất, trả về nốt được add
TreeNode* addRight(TreeType *root,bTreeElementtype e)
{
    TreeNode* node = makeTreeNode(e);
    if(node==NULL) return node;
    if(*root==NULL) *root = node;
    else
    {
        TreeNode* RNode = *root;
        while(RNode->right != NULL)
        RNode = RNode->right;
        RNode->right = node;
    }
    return node;
}

int numberOfRightChildren(TreeType root)
{
    if (root == NULL) return 0;
    if (rightTree(root) != NULL && isLeaf(rightTree(root))) return 1 + numberOfRightChildren(leftTree(root));
    if (leftTree(root) != NULL) return numberOfRightChildren(leftTree(root)) + numberOfRightChildren(rightTree(root)) + 1;
    else return numberOfRightChildren(leftTree(root));
}

void bInsert(TreeType *root,bTreeElementtype e)
{
  TreeNode *new;
  if ((*root) == NULL)
    {
      (*root) = makeTreeNode(e);
      return;
    }
  if (insCompare((*root)->element, e) < 0) bInsert(&((*root)->right), e);
  else bInsert(&((*root)->left), e);
}

//in theo thứ tự tăng dần
void inOrderPrint(TreeType root)
{
  if (root == NULL) return;
  inOrderPrint(root->left);
  printElement(root->element);
  inOrderPrint(root->right);
}

//in theo thứ tự giảm dần
void preOrderPrint(TreeType root)
{
  if (root == NULL) return;
  preOrderPrint(root->right);
  printElement(root->element);
  preOrderPrint(root->left);
}

TreeNode *bSearch(TreeType root,bTreeElementtype e)
{
  if (root == NULL) return NULL;
  if(searchCompare(root->element,e)==0) return root;
  if(bSearch(root->left,e)!=NULL) return bSearch(root->left,e);
  if(bSearch(root->right,e)!=NULL) return bSearch(root->right,e);
  return NULL;
}

//tìm trái nhất,key nhỏ nhất
TreeNode *findMin(TreeType root)
{
  if (root == NULL) return NULL;
  while (root->left != NULL) root = root->left;
  return root;
}

//tìm phải nhất,key lớn nhất
TreeNode *findMax(TreeType root)
{
  if (root == NULL) return NULL;
  while (root->right != NULL) root = root->right;
  return root;
}

void deleteNode(TreeType* root,bTreeElementtype e)
{
        TreeNode* temp;
        if(*root==NULL)
        {
                return;
        }
        /*
        else if(delCompare((*root)->element, e) < 0)
        {

            deleteNode(&(*root)->left, e);
        }
        else if(delCompare((*root)->element, e) >0)
        {
            deleteNode(&(*root)->right, e);
        }
        */
        if(delCompare((*root)->element,e)!=0)
        {
            if((*root)->left!= NULL) deleteNode(&(*root)->left, e);
            if((*root)->right!=NULL) deleteNode(&(*root)->right, e);
        }
        else
        {
            // có cả 2 con trái và phải, tìm min cây con phải và đổi chỗ, r xóa trên cây con phải
	    if((*root)->right && (*root)->left)
	      {
            temp = findMin((*root)->right);
            (*root)->element = temp->element;
            deleteNode(&(*root)->right,temp->element);
	      }
	    else
	      {
            temp = (*root);
            if((*root)->left == NULL) //có con phải, k có con trái
            (*root) = (*root)->right;
            else if((*root)->right == NULL)
            (*root) = (*root)->left;
            free(temp);
	      }
	  }
}

void deleteTree(TreeType* root)
{
    while((*root) !=NULL)
        deleteNode(root,(*root)->element);
}

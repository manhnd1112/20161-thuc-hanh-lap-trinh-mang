#include<stdio.h>

typedef struct
{
    char name[30];
    char mssv[8];
    float diem;
}bTreeElementtype;


int insCompare(bTreeElementtype e1, bTreeElementtype e2)
{
    if(e1.diem == e2.diem) return 0;
    if(e2.diem > e1.diem) return -1;
    if(e2.diem < e1.diem) return 1;
}

int searchCompare(bTreeElementtype e1,bTreeElementtype e2)
{
    return strcmp(e1.mssv,e2.mssv);
}

int delCompare(bTreeElementtype e1,bTreeElementtype e2)
{
    return strcmp(e1.mssv,e2.mssv);
}

void printElement(bTreeElementtype e)
{
    printf("%-15s%-30s%-10.2f\n",e.mssv,e.name,e.diem);
}

bTreeElementtype* scanfElement()
{
    bTreeElementtype* e;
    e = (bTreeElementtype *)malloc(sizeof(bTreeElementtype));
    printf("Nhap ten: ");
    scanf("%[^\n]%*c",e->name);
    printf("Nhap mssv: ");
    scanf("%[^\n]%*c",e->mssv);
    printf("Nhap diem: ");
    scanf("%f%*c",&e->diem);
    return e;
}

bTreeElementtype* putElement(char* name,char* mssv,float diem)
{
    bTreeElementtype* e;
    e = (bTreeElementtype *)malloc(sizeof(bTreeElementtype));
    strcpy(e->name,name);
    strcpy(e->mssv,mssv);
    e->diem = diem;
    return e;
}

#include"btree.h"

int main()
{
    int i,j,k,ch,n;
    bTreeElementtype* e;
    TreeType root;
    TreeNode* node;
    makeTree(&root);
    e = putElement("manh","20132524",8.5);
    bInsert(&root,*e);
    e = putElement("minh","20132605",8.2);
    bInsert(&root,*e);
    e = putElement("tao","20131234",8.7);
    bInsert(&root,*e);
    e = putElement("hai","20132345",8.0);
    bInsert(&root,*e);
    e = putElement("ky","20133456",8.3);
    bInsert(&root,*e);
    e = putElement("long","20134567",8.4);
    bInsert(&root,*e);
    while(ch!=20)
    {
        printf("\n1. Insert\n");
        printf("2. Search Node\n");
        printf("3. Left Tree,Right Tree\n");
        printf("4. is Leaf?\n");
        printf("5. Is Empty Tree\n");
        printf("6. Number Node\n");
        printf("7. Number Leaf\n");
        printf("8. Number Internal Node\n");
        printf("9. Heigh Tree\n");
        printf("10. creat from 2 tree\n");
        printf("11. add Left\n");
        printf("12. add Right\n");
        printf("13. deleteNode\n");
        printf("14. find Min\n");
        printf("15. find Max\n");
        printf("16. delete Tree\n");
        printf("17. inOrder Print\n");
        printf("18.  preOrder Print\n");
        printf("20. Thoat\n");
        printf("Nhap lua chon: ");
        scanf("%d%*c",&ch);
        while(ch<1&&ch>20)
        {
            printf("Sai lua chon. Nhap lai: ");
            scanf("%d%*c",&ch);
        }
        switch(ch)
        {
        case 1:
            e = scanfElement();
            bInsert(&root,*e);
            break;
        case 2:
            e = (bTreeElementtype *)malloc(sizeof(bTreeElementtype));
            printf("Nhap mssv tim kiem: ");
            scanf("%[^\n]%*c",&e->mssv);
            node = bSearch(root,*e);
            if(node == NULL)
                printf("Khong tim thay\n");
            else
                printElement(node->element);
            break;
        case 3:
            e = (bTreeElementtype *)malloc(sizeof(bTreeElementtype));
            printf("Nhap mssv: ");
            scanf("%[^\n]%*c",&e->mssv);
            node = bSearch(root,*e);
            if(node == NULL)
                printf("Khong tim thay\n");
            else
            {
                printf("__Left Tree \n");
                if(leftTree(node)== NULL) printf("NULL\n");
                else
                printElement(leftTree(node)->element);
                printf("__Right Tree\n");
                if(rightTree(node)== NULL) printf("NULL\n");
                else
                printElement(rightTree(node)->element);
            }
            break;
        case 4:
            e = (bTreeElementtype *)malloc(sizeof(bTreeElementtype));
            printf("Nhap mssv: ");
            scanf("%[^\n]%*c",&e->mssv);
            node = bSearch(root,*e);
            if(node == NULL)
                printf("Khong tim thay\n");
            else
            {
                if(isLeaf(node)) printf("YES\n");
                else
                    printf("NO\n");
            }
            break;
        case 5:
            if(isEmptyTree(root)) printf("YES\n");
            else
                printf("NO\n");
            break;
        case 6:
            printf("Number Node: %d\n",numberNodeTree(root));
            break;
        case 7:
            printf("Number Leaf: %d\n",numberOfLeaf(root));
            break;
        case 8:
            printf("Number Internal Node: %d\n",numberOfInternalNode(root));
            break;
        case 9:
            printf("Height Tree: %d\n",heightOfTree(root));
            break;
        case 10:
            break;
        case 11:
            e = scanfElement();
            addLeft(&root,*e);
            break;
        case 12:
            e = scanfElement();
            addRight(&root,*e);
            break;
        case 13:
            e = (bTreeElementtype *)malloc(sizeof(bTreeElementtype));
            printf("Nhap mssv del: ");
            scanf("%[^\n]%*c",&e->mssv);
            node = bSearch(root,*e);
            deleteNode(&root,*e);
            /*
            if(node == NULL)
                printf("Khong tim thay\n");
            else
            {
                deleteNode(&root,node->element);
                printf("Xoa thanh cong!!!\n");
                if(root!=NULL)
                {
                    printf("ROOT moi\n");
                    printElement(root->element);
                }
            }
            */
            break;
        case 14:
            node = findMin(root);
            if(node == NULL) printf("Not Found\n");
            else
                printElement(node->element);
            break;
        case 15:
            node = findMax(root);
            if(node == NULL) printf("Not Found\n");
            else
                printElement(node->element);
            break;
        case 16:
            deleteTree(&root);
            printf("Xoa thanh cong!!!\n");
            break;
        case 17:
            inOrderPrint(root);
            break;
        case 18:
            preOrderPrint(root);
            break;
        case 20:
            getch();
            return 0;
        }
    }
    getch();
    return 0;
}

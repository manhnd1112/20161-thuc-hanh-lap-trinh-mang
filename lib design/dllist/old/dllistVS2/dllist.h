#include<stdio.h>
#include<conio.h>
#include<string.h>

typedef struct DllistElementtype
{
    char name[100];
    char sdt[20];
}DllistElementtype;
typedef struct DllistNode DllistNode;

struct DllistNode
{
    DllistElementtype element;
    DllistNode *next;
    DllistNode *prev;
};

typedef struct
{
    DllistNode *head;
    DllistNode *tail;
    DllistNode *cur;
}DllistType;

/////////////////////////////////
void printfDllistNode(DllistElementtype e);
int func(DllistElementtype e1,DllistElementtype e2); // for SEARCH
int sosanhDllist(DllistNode *node1, DllistNode *node2); // for SORT
/////////////////////////////////
DllistNode* makeNodeDllist(DllistElementtype e);
DllistType* makeDllist();
void traverseDllist(DllistType* li);
void reverseDllist(DllistType* li);

int numberNodeDllist(DllistType* li);
int hasPositionDllist(DllistType* li,int n);
DllistNode* findAtPositionDllist(DllistType* li,int n);

void insertAtBeginningDllist(DllistType* li, DllistElementtype e);
void insertAtEndDllist(DllistType *li,DllistElementtype e);
void insertAfterDllist(DllistType* li,DllistElementtype e);
void insertBeforeDllist(DllistType* li,DllistElementtype e);
void insertAtPositionDlllist(DllistType* li,DllistElementtype e,int n);

void deleteAtBeginningDllist(DllistType* li);
void deleteAtEndDllist(DllistType* li);
void deleteCurDllist(DllistType* li);
void deleteAtPositionDllist(DllistType* li,int n);
void deleteNodeDllist(DllistType *li,DllistNode* node);
void deleteListDllist(DllistType* li);

DllistNode* searchDllist(DllistType* li,DllistElementtype e);
void updateDllist(DllistNode* node,DllistElementtype e);
void swapDllist(DllistNode *n1,DllistNode *n2);
void quicksortDllist(DllistType* li);
void quicksort2Dllist(DllistNode **A, long l, long r);

#include<stdio.h>
#include<conio.h>
#include<string.h>
#include<stdlib.h>
#include"dllist.h"
DllistNode* makeNodeDllist(DllistElementtype e)
{
    DllistNode *new;
    new = (DllistNode *)malloc(sizeof(DllistNode));
    if (new == NULL)
    {
      printf("Khong cap phat duoc bo nho\n");
      return NULL;
    }
    new->element = e;
    new->prev = NULL;
    new->next = NULL;
    return new;
}

DllistType* makeDllist()
{
    DllistType* li;
    li = (DllistType *)malloc(sizeof(DllistType));
    if(li==NULL)
    {
        printf("Khong cap phat duoc bo nho cho List\n");
        return NULL;
    }
    else
    {
        li->head = NULL;
        li->tail = NULL;
        li->cur = NULL;
        return li;
    }
}

void traverseDllist(DllistType* li)
{
    DllistNode * new;
    if(li->head==NULL)
    {
        printf("Danh sach trong!\n");
    }else
    for(new = li->head; new!=NULL; new = new->next)
    {
        printfDllistNode(new->element);
    }
}

void reverseDllist(DllistType* li)
{
    DllistNode * new;
    if(li->tail==NULL)
    {
        printf("Danh sach trong!\n");
    }else
    for(new = li->tail; new!=NULL; new = new->prev)
    {
        printfDllistNode(new->element);
    }
}

int numberNodeDllist(DllistType* li)
{
    int i = 0;
    for (li->cur = li->head; li->cur != NULL; li->cur = li->cur->next,i++);
    return i;
}

int hasPositionDllist(DllistType* li,int i)
{
    int n;
    n = numberNodeDllist(li);
    if(n==0) return 0;
    if(i <= 0 || i > n) return 0;
    return 1;
}

DllistNode* findAtPositionDllist(DllistType* li,int n)
{
    if (hasPositionDllist(li, n) == 0)
    {
      printf("Khong ton tai phan tu %d trong list\n",n);
      return NULL;
    }
    int i = 0;
    for (li->cur=li->head; li->cur != NULL; li->cur=li->cur->next)
    {
      i++;
      if (i == n) break;
      //i++;
    }
    return li->cur;
}

void insertAtBeginningDllist(DllistType *li,DllistElementtype e)
{
    DllistNode *new;
    new = makeNodeDllist(e);
    if (li->head == NULL)
    {
        li->head = new;
        li->tail = new;
        li->cur = new;
        return;
    }
    if ((li->head)->prev != NULL)
    {
      printf("Con tro head cua list sai\n");
      return;
    }
    new->next = li->head;
    li->head->prev = new;
    li->cur = new;
    li->head = new;
}


void insertAtEndDllist(DllistType *li,DllistElementtype e)
{
    DllistNode *new;
    new = makeNodeDllist(e);
    if (li->head == NULL)
    {
        li->head = new;
        li->tail = new;
        li->cur = new;
        return;
    }
    if ((li->tail)->next != NULL)
    {
      printf("Con tro tail cua list sai\n");
      return;
    }
    li->tail->next = new;
    new->prev = li->tail;
    li->tail = new;
}

void insertAfterdllist(DllistType* li,DllistElementtype e)
{
    DllistNode *new;
    new = makeNodeDllist(e);
    if (li->head == NULL)
    {
        li->head = new;
        li->tail = new;
        li->cur = new;
        return;
    }
    if (li->cur == li->tail) insertAtEndDllist(li, e);
    else
    {
      new->next = li->cur->next;
      new->prev = li->cur;
      li->cur->next = new;
      if (new->next != NULL) new->next->prev = new;
    }
    li->cur = new;
}

void insertBeforeDllist(DllistType* li,DllistElementtype e)
{
    DllistNode *new;
    new = makeNodeDllist(e);
    if (li->head == NULL)
    {
        li->head = new;
        li->tail = new;
        li->cur = new;
        return;
    }
    if (li->cur == li->head) insertAtBeginningDllist(li,e);
    else
    {
        new->next = li->cur;
        new->prev = li->cur->prev;
        li->cur->prev = new;
        if (new->prev != NULL) new->prev->next = new;
        }
    li->cur = new;
}

void insertAtPositionDlllist(DllistType* li,DllistElementtype e,int n)
{
    li->cur = findAtPositionDllist(li, n);
    if (li->cur == NULL)
    {
        printf("insertatposition is error\n");
        return;
    }
    insertAtBeginningDllist(li, e);
}

void deleteAtBeginningDllist(DllistType* li)
{
    if (li->head == NULL) return;
    if ((li->head)->prev != NULL)
    {
        printf("Con tro head cua list sai\n");
        exit(0);
    }
    DllistNode *temp;
    temp = li->head;
    li->head = (li->head)->next;
    if (li->head != NULL) (li->head)->prev = NULL;
    if (li->head == NULL) li->tail = NULL;
    free(temp);
}

void deleteAtEndDllist(DllistType* li)
{
    if (li->tail == NULL) return;
    if ((li->tail)->next != NULL)
    {
        printf("Con tro tail cua list sai\n");
        exit(0);
    }
    DllistNode *temp;
    temp = (li->tail)->prev;
    if (temp != NULL) temp->next = NULL;
    free(li->tail);
    li->tail = temp;
    if (li->tail == NULL) li->head = NULL;
}

void deleteCurDllist(DllistType* li)
{
    if (li->cur == NULL) return;
    if (li->cur == li->head) deleteAtBeginningDllist(li);
    else
    if (li->cur == li->tail) deleteAtEndDllist(li);
    else
    {
        DllistNode *temp;
        li->cur->prev->next = li->cur->next;
        li->cur->next->prev = li->cur->prev;
        temp = li->cur->next;
        free(li->cur);
        li->cur = temp;
    }
}

void deleteAtPositionDllist(DllistType* li,int n)
{
    li->cur = findAtPositionDllist(li, n);
    if (li->cur == NULL)
    {
      printf("deleteatposition is error\n");
      return;
    }
    deleteCurDllist(li);
}


void deleteNodeDllist(DllistType *li,DllistNode* node)
{
    if (node == NULL) return;
    if (node == li->head) deleteAtBeginningDllist(li);
    else
    if (node == li->tail) deleteAtEndDllist(li);
    else
    {
        node->prev->next = node->next;
        node->next->prev = node->prev;
        free(node);
    }
}

void deleteListDllist(DllistType* li)
{
    while(li->head!=NULL)
        deleteAtBeginningDllist(li);
}

DllistNode* searchDllist(DllistType* li,DllistElementtype e)
{
    li->cur = li->head;
    while (li->cur != NULL)
    {
      if(func(li->cur->element,e)==0) break;
      li->cur = li->cur->next;
    }

    return li->cur;
}

void updateDllist(DllistNode* node,DllistElementtype e)
{
    node->element = e;
}


void swapDllist(DllistNode *n1,DllistNode *n2)
{
  DllistElementtype a;
  a = n1->element;
  n1->element = n2->element;
  n2->element = a;
}

void quicksortDllist(DllistType* li)
{
  long n, i = 0;
  DllistNode **A;
  n = numberNodeDllist(li);
  A = (DllistNode **)malloc(n * sizeof(DllistNode*));
  for (li->cur = li->head; li->cur != NULL; li->cur = li->cur->next)
    {
      A[i] = li->cur;
      i++;
    }
  quicksort2Dllist(A, 0, i-1);
}

void quicksort2Dllist(DllistNode **A, long l, long r)
{
  long i, j;
  DllistNode *p;
  if (l < r)
    {
      p = A[l]; i = l; j = r;
      while (i < j)
	{
	  i++;
	  while (i < r && sosanhDllist(A[i], p) < 0) i++;
	  while (j > l && sosanhDllist(A[j], p) > 0) j--;
	  swapDllist(A[i], A[j]);
	}
      swapDllist(A[i], A[j]); swapDllist(A[l], A[j]);
      quicksort2Dllist(A,l,j-1);
      quicksort2Dllist(A,j+1,r);
    }
}

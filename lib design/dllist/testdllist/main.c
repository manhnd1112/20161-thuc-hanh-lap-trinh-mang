#include<stdio.h>
#include<conio.h>
#include<string.h>
#include<stdlib.h>

typedef struct
{
    char name[30];
    char sdt[20];
}DllistElementtype;

#include"../dllist.h"

void printfDllistNode(DllistElementtype e)
{
    printf("%-30s%-15s\n",e.name,e.sdt);
}

int func(DllistElementtype e1,DllistElementtype e2)
{
    return strcmp(e1.name,e2.name);
}

int sosanhDllist(DllistNode *node1, DllistNode *node2)
{
    return strcmp(node1->element.name,node2->element.name);
}

DllistElementtype* scanfDllistElementtype()
{
    DllistElementtype *e;
    e = (DllistElementtype *)malloc(sizeof(DllistElementtype));
    printf("Nhap ho ten: ");
    scanf("%[^\n]%*c",e->name);
    printf("Nhap sdt: ");
    scanf("%[^\n]",e->sdt);
    return e;
}



int main()
{
    DllistType *li;
    int end = 20;
    int ch;
    int i,n;
    li = makeDllist();
    DllistNode * node;
    DllistElementtype *e;

    while(ch!=end)
    {
        printf("Menu\n");
        printf("1. Traverse List\n");
        printf("2. Reverse List\n");
        printf("3. So phan tu\n");
        printf("4. Kiem tra ton tai vi tri?\n");
        printf("5. Tim phan tu tai vi tri\n");
        printf("6. Insert at Beginning\n");
        printf("7. Insert at End\n");
        printf("8. Insert After\n");
        printf("9. Insert Before\n");
        printf("10. Insert at position\n");
        printf("11. Delete at Beginning\n");
        printf("12. Delete at End\n");
        printf("13. Delete at Cur\n");
        printf("14. Delete at Position\n");
        printf("15. Delete Node\n");
        printf("16. Delete List\n");
        printf("17. Tim node\n");
        printf("18. Update Node\n");
        printf("19. Sort\n");
        printf("%d. Exit\n\n",end);
        printf("Nhap lua chon: ");
        scanf("%d%*c",&ch);
        while(ch<1 || ch >end)
        {
            printf("Khong co su lua chon nao. Nhap lai: ");
            scanf("%d%*c",&ch);
        }
        switch(ch)
        {
            case 1:
                traverseDllist(li);
                break;
            case 2:
                reverseDllist(li);
                break;
            case 3:
                printf("So phan tu: %d\n",numberNodeDllist(li));
                break;
            case 4:
                printf("Nhap vi tri: ");
                scanf("%d%*c",&n);
                if(hasPositionDllist(li,n)==1)
                    printf("Ton tai vi tri\n");
                else
                    printf("Khong ton tai\n");
                break;
            case 5:
                printf("Nhap vi tri: ");
                scanf("%d%*c",&n);
                node = findAtPositionDllist(li,n);
                if(node==NULL)
                    printf("Khong tim thay\n");
                else
                {
                    printf("Tim thay!\n");
                    printfDllistNode(node->element);
                }
                break;
            case 6:
                e=scanfDllistElementtype();
                insertAtBeginningDllist(li,*e);
                break;
            case 7:
                e=scanfDllistElementtype();
                insertAtEndDllist(li,*e);
                break;
            case 8:
                e=scanfDllistElementtype();
                insertAfterdllist(li,*e);
                break;
            case 9:
                e=scanfDllistElementtype();
                insertBeforeDllist(li,*e);
                break;
            case 10:
                printf("Nhap vi tri: ");
                scanf("%d%*c",&n);
                e=scanfDllistElementtype();
                insertAtPositionDlllist(li,*e,n);
                break;
            case 11:
                deleteAtBeginningDllist(li);
                break;
            case 12:
                deleteAtEndDllist(li);
                break;
            case 13:
                deleteCurDllist(li);
                break;
            case 14:
                printf("Nhap vi tri can xoa: ");
                scanf("%d%*c",&n);
                deleteAtPositionDllist(li,n);
                break;
            case 15:
                e=(DllistElementtype *)malloc(sizeof(DllistElementtype));
                printf("Nhap ten can tim kiem: ");
                scanf("%[^\n]",e->name);
                node = searchDllist(li,*e);
                if(node== NULL) printf("khong tim thay");
                else
                {
                    printf("Tim thay\n");
                    printfDllistNode(node->element);
                    printf("Da xoa node\n");
                    deleteNodeDllist(li,node);
                }


                break;
            case 16:
                deleteListDllist(li);
                break;
            case 17:
                e=(DllistElementtype *)malloc(sizeof(DllistElementtype));
                printf("Nhap ten can tim kiem: ");
                scanf("%[^\n]",e->name);
                node = searchDllist(li,*e);
                if(node== NULL) printf("khong tim thay");
                else
                {
                    printf("Tim thay\n");
                    printfDllistNode(node->element);

                }
                break;

            case 18:
                e=(DllistElementtype *)malloc(sizeof(DllistElementtype));
                printf("Nhap ten can tim kiem: ");
                scanf("%[^\n]%*c",e->name);
                node = searchDllist(li,*e);
                if(node== NULL) printf("khong tim thay\n");
                else
                {
                    printf("Tim thay\n");
                    printf("Nhap thong tin update: \n");
                    updateDllist(node,*scanfDllistElementtype());

                }
                break;

            case 19:
                quicksortDllist(li);
                break;
            case 20:
                getch();
                return 0;
                break;
        }
    }
}

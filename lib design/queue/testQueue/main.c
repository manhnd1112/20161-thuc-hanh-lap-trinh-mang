#include<stdio.h>
#include<stdlib.h>

typedef struct
{
    char name[30];
    char email[20];
}QueueElementtype;

#include"../queue.h"

void DisplayNodeQueue(QueueElementtype e)
{
    printf("%-30s%-20s\n",e.name,e.email);
}

QueueElementtype scanfElement()
{
    QueueElementtype *e;
    e = (QueueElementtype *)malloc(sizeof(QueueElementtype));
    printf("Nhap ten: ");
    scanf("%[^\n]%*c",e->name);
    printf("Nhap email: ");
    scanf("%[^\n]%*c",e->email);
    return *e;
}
int main()
{
    int ch;
    QueueType q;
    q = makeQueue();
    QueueElementtype *e;
    while(ch!=8)
    {
        printf("1. EnQueue\n");
        printf("2. DeQueue\n");
        printf("3. Get Head Element\n");
        printf("4. Get Tail Element\n");
        printf("5. Is Empty Queue?\n");
        printf("6. Delete Queue\n");
        printf("7. Duyet Queue\n");
        printf("8. Exit\n\n");
        printf("Nhap lua chon: ");
        scanf("%d%*c",&ch);
        while(ch<1 || ch>8)
        {
            printf("Khong co lua chon nao. Nhap lai: ");
            scanf("%d%*c",&ch);
        }
        switch(ch)
        {
            case 1:
                enQueue(&q,scanfElement());
                break;
            case 2:
                e = (QueueElementtype *)malloc(sizeof(QueueElementtype));
                e = deQueue(&q);
                if(e==NULL) printf("Queue Empty\n");
                else
                    DisplayNodeQueue(*e);
                break;
            case 3:
                e = (QueueElementtype *)malloc(sizeof(QueueElementtype));
                e = getHeadElementQueue(q);
                if(e==NULL) printf("Queue Empty\n");
                else
                {
                  DisplayNodeQueue(*e);
                  printf("Dia chi: %p va dia chi head: %p\n",e,&q.head->element);
                }
                break;
            case 4:
                e = (QueueElementtype *)malloc(sizeof(QueueElementtype));
                e = getTailElementQueue(q);
                if(e==NULL) printf("Queue Empty\n");
                else
                {
                  DisplayNodeQueue(*e);
                  printf("Dia chi: %p va dia chi tail: %p\n",e,&q.tail->element);
                }
                break;
            case 5:
                if(isEmptyQueue(q)) printf("Queue Empty\n");
                else
                    printf("Queue is not Empty\n");
                break;
            case 6:
                deleteQueue(&q);
                break;
            case 7:
                traverseQueue(q);
                break;
            case 8:
                return 0;
                break;

        }
    }
}


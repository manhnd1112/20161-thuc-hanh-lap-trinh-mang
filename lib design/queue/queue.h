#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
#include<conio.h>
#include<string.h>

typedef struct QueueNode QueueNode;
typedef struct QueueType QueueType;

struct QueueNode
{
    QueueElementtype element;
    QueueNode* next;
};

struct QueueType
{
    QueueNode* head;
    QueueNode* tail;
};

void DisplayNodeQueue(QueueElementtype e); // dung cho ham duyet traverseQueue

QueueType makeQueue();
void enQueue(QueueType* q,QueueElementtype e);
QueueElementtype* deQueue(QueueType* q);
QueueElementtype* getHeadElementQueue(QueueType q);
QueueElementtype* getTailElementQueue(QueueType q);
bool isEmptyQueue(QueueType q);
void deleteQueue(QueueType *q);
void traverseQueue(QueueType q);

QueueType makeQueue()
{
    QueueType *q;
    q = (QueueType *)malloc(sizeof(QueueType));
    q->head = NULL;
    q->tail = NULL;
    return *q;
}

void enQueue(QueueType* q,QueueElementtype e)
{
    QueueNode *node;
    node = (QueueNode *)malloc(sizeof(QueueNode));
    if(node == NULL)
    {
        printf("Khong cap phat duoc bo nho\n");
        return;
    }
    node->element = e;
    node->next = NULL;
    if(q->tail!=NULL)
    {
        q->tail->next = node;
        q->tail = node;
    }
    else
    {
        q->head = node;
        q->tail = node;
    }
}

QueueElementtype* deQueue(QueueType *q)
{
    if(q->head==NULL) return NULL;
    QueueNode *node;
    node = q->head;
    if(q->head->next!= NULL) q->head = q->head->next;
    else
    {
        q->head = NULL;
        q->tail = NULL;
    }
    return &(node->element);
}

QueueElementtype* getHeadElementQueue(QueueType q)
{
    QueueElementtype *e;
    if(q.head==NULL) return NULL;
    else
    {
        e = (QueueElementtype *)malloc(sizeof(QueueElementtype));
        *e = q.head->element;
        return e;
    }
}

QueueElementtype* getTailElementQueue(QueueType q)
{
    QueueElementtype *e;
    if(q.tail==NULL) return NULL;
    else
    {
        e = (QueueElementtype *)malloc(sizeof(QueueElementtype));
        *e = q.tail->element;
        return e;
    }
}

bool isEmptyQueue(QueueType q)
{
    return q.head == NULL;
}

void deleteQueue(QueueType* q)
{
    QueueNode* node;
    if(isEmptyQueue(*q)) return;
    while(isEmptyQueue(*q)==false)
    {
        node = q->head;
        q->head = node->next;
        free(node);
    }
}

void traverseQueue(QueueType q)
{
    if(q.head==NULL) return;
    QueueNode* node;
    node = q.head;
    do
    {
        DisplayNodeQueue(node->element);
        node = node->next;
    }while(node!=NULL);
}

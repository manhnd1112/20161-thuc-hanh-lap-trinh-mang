#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
#include<conio.h>
#include<string.h>

typedef struct StackNode StackNode;
typedef struct StackType StackType;

struct StackNode
{
    StackElementtype element;
    StackNode* next;
};

struct StackType
{
    StackNode* top;
};

void DisplayNodeStack(StackElementtype e);// dung cho ham duyet traverseStack

StackType makeStack();
void pushStack(StackType* sta,StackElementtype);
StackElementtype* popStack(StackType* sta);
bool isEmptyStack(StackType sta);
StackElementtype* getElementTopStack(StackType sta);
void deleteStack(StackType* sta);
void traverseStack(StackType sta);


StackType makeStack()
{
    StackType* sta;
    sta = (StackType *)malloc(sizeof(StackType));
    sta->top = NULL;
    return *sta;
}
void pushStack(StackType* sta,StackElementtype e)
{
    StackNode* node;
    node = (StackNode *)malloc(sizeof(StackNode));
    if(node==NULL)
    {
        //printf("Khong cap phat duoc bo nho\n");
        return;
    }
    node->element = e;
    node->next = NULL;
    if(sta->top!=NULL) node->next = sta->top;
    sta->top = node;
}

StackElementtype* popStack(StackType* sta)
{
    if(sta->top == NULL) return NULL;
    StackNode* node;
    StackElementtype e;
    node = sta->top;
    sta->top = sta->top->next;
    e = node->element;
    return &e;
}

bool isEmptyStack(StackType sta)
{
    return sta.top == NULL;
}

StackElementtype* getElementTopStack(StackType sta)
{
    StackElementtype *e;

    if(sta.top == NULL) return NULL;
    else
    {
        e = (StackElementtype *)malloc(sizeof(StackElementtype));
        *e = sta.top->element;
        return e;
    }
}

void deleteStack(StackType* sta)
{
    if(sta->top == NULL) return;
    StackNode* node;
    while(isEmptyStack(*sta)==false)
    {
        node = sta->top;
        sta->top = sta->top->next;
        free(node);
    }
}

void traverseStack(StackType sta)
{
    if(sta.top == NULL) return;
    StackNode* node;
    node = sta.top;
    do
    {
        DisplayNodeStack(node->element);
        node = node->next;
    }while(node!=NULL);
}


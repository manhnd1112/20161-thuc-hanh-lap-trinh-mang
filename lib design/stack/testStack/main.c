#include<stdio.h>
#include<conio.h>
#include<string.h>

typedef struct
{
    char name[30];
    char email[20];
}StackElementtype;

#include"../stack.h"

void DisplayNodeStack(StackElementtype e)
{
    printf("%-30s%-20s\n",e.name,e.email);
}

StackElementtype scanfElement()
{
    StackElementtype *e;
    e = (StackElementtype *)malloc(sizeof(StackElementtype));
    printf("Nhap ten: ");
    scanf("%[^\n]%*c",e->name);
    printf("Nhap email: ");
    scanf("%[^\n]%*c",e->email);
    return *e;
}


int main()
{
    int ch;
    StackType sta;
    StackElementtype* e;
    sta = makeStack();
    while(ch!=7)
    {
        printf("Menu\n");
        printf("1. Push\n");
        printf("2. Pop\n");
        printf("3. Stack is Empty?\n");
        printf("4. Get element of Top\n");
        printf("5. Delete Stack\n");
        printf("6. Duyet Stack\n");
        printf("7. Exit\n\n");
        printf("Nhap lua chon: ");
        scanf("%d%*c",&ch);
        while(ch<1 || ch>7)
        {
            printf("Khong co lua chon nao. Nhap lai: ");
            scanf("%d%*c",&ch);
        }
        switch(ch)
        {
        case 1:
            pushStack(&sta,scanfElement());
            break;
        case 2:
            e= popStack(&sta);
            break;
        case 3:
            if(isEmptyStack(sta)) printf("Stack Empty\n");
            else
                printf("Stack is not Empty\n");
            break;
        case 4:
            if(isEmptyStack(sta)) printf("Stack is Empty\n");
            else
            {
                e = getElementTopStack(sta);
                DisplayNodeStack(*e);
            }

            break;
        case 5:
            deleteStack(&sta);
            printf("add");
            break;

        case 6:
            traverseStack(sta);
            break;
        case 7:
            return 0;
            break;
        }
    }

}


#include<stdio.h>
#include<conio.h>

typedef struct SllistNode SllistNode;

struct SllistNode
{
    elementtype element;
    SllistNode *next;
};

typedef struct
{
    SllistNode *root;
    SllistNode *cur;
    SllistNode *prev;
}SllistType;
///////////////////////////////////////
void printfSllistNode(elementtype element);
int func(elementtype e1, elementtype e2);
///////////////////////////////////////
SllistNode* makeNodeSllist(elementtype element);
SllistType* makeSllist();
void traverseSllist(SllistType* li);
void insertAfterSllist(SllistType* li,elementtype element);
void insertBeforeSllist(SllistType* li,elementtype element);
void insertAtBeginningSllist(SllistType* li,elementtype element);
void insertAtEndSllist(SllistType *li,elementtype element);
void insertAtPositionSlllist(SllistType* li,elementtype element,int n); //xet vi tri tu 0, neu xet vi tri tu 1 thi dau vao la n-1
void deleteAtBeginning(SllistType *li);
void deleteAtEnd(SllistType *li);
void deleteCur(SllistType *li);
void deleteList(SllistType *li);
void deleteAtPosition(SllistType* li, int n);
SllistNode *searchFunc(SllistType *li,elementtype element);

///////////////////////////////////////
SllistNode* makeNodeSllist(elementtype element)
{
    SllistNode *new;
    new = (SllistNode *)malloc(sizeof(SllistNode));
    if(new==NULL)
    {
        printf("Khong cap phat duoc bo nho\n");
        return NULL;
    }
    new->element = element;
    new->next = NULL;
    return new;
}


SllistType* makeSllist()
{
    SllistType *li;
    li = (SllistType *)malloc(sizeof(SllistType));
    if(li==NULL)
    {
        printf("Loi. Khong cap phat duoc bo nho de tao Single Link List\n");
        return NULL;
    }
    else
    {
        li->root = NULL;
        li->cur = NULL;
        li->prev = NULL;
        return li;
    }

}

void traverseSllist(SllistType* li)
{
    SllistNode * new;
    if(li->root==NULL)
    {
        printf("Danh sach trong!\n");
    }else
    for(new = li->root; new!=NULL; new = new->next)
    {
        printfSllistNode(new->element);
    }
}

void insertAfterSllist(SllistType* li,elementtype element)
{
    SllistNode *new;
    new = makeNodeSllist(element);
    if(li->root==NULL)
    {
        li->root = new;
        li->cur = li->root;
        li->prev = NULL;
    }else
    {
        new->next = li->cur->next;
        li->cur->next = new;
        li->prev = li->cur;
        li->cur = new;
    }
}

void insertBeforeSllist(SllistType* li,elementtype element)
{
    SllistNode *new;
    new = makeNodeSllist(element);
    if(li->root==NULL)
    {
        li->root = new;
        li->cur = li->root;
        li->prev = NULL;
    }else
    {
        new->next = li->cur;
        if(li->prev!=NULL) li->prev->next = new;
        if(li->cur==li->root) li->root = new;
        li->cur = new;
    }
}

void insertAtBeginningSllist(SllistType* li,elementtype element)
{
    SllistNode* new;
    new = makeNodeSllist(element);
    new->next = li->root;
    li->root = new;
    li->cur = new;
    li->prev = NULL;
}

void insertAtEndSllist(SllistType *li,elementtype element)
{
    SllistNode* new;
    SllistNode* node;
    new = makeNodeSllist(element);
    if(li->root==NULL)
    {
        li->root = new;
        li->cur = li->root;
        li->prev = NULL;
    }else
    {
        for(node=li->root ; node->next!=NULL ; node=node->next);
        li->prev = node;
        node->next = new;
        li->cur = new;
    }
}

void insertAtPositionSlllist(SllistType* li,elementtype element,int n)
{
    int i = 0;
    SllistNode *prev2;
    li->prev = NULL;
    for (li->cur=li->root; li->cur != NULL; li->cur=li->cur->next)
        {
        if (i == n) break;
        i++;
        prev2 = li->prev;
        li->prev = li->cur;
        }
    if(li->cur == NULL)
        {
        li->cur = li->prev;
        li->prev = prev2;
        printf("Loi, khong co phan tu thu %d trong danh sach\n",n);
        return;
        }
    insertBeforeSllist(li,element);
}

void deleteAtBeginning(SllistType *li)
{
    SllistNode *temp;
    temp = li->root;
    if (li->root == NULL) return;
    if (li->cur == li->root) li->cur = li->root->next;
    li->root = li->root->next;
    free(temp);
}

void deleteAtEnd(SllistType *li)
{
    SllistNode *prev2;
    if (li->root == NULL) return;
    for (li->cur = li->root; li->cur->next != NULL; li->cur = li->cur->next)
    {
        prev2 = li->prev;
        li->prev = li->cur;
    }
    free(li->cur);
    li->cur = li->prev;
    li->prev = prev2;
    if (li->cur != NULL) li->cur->next = NULL;
}

void deleteCur(SllistType *li)
{
    if (li->root == NULL) return;
    if (li->cur == li->root) deleteAtBeginning(li);
    else
    if (li->cur->next == NULL) deleteAtEnd(li);
    else
    {
        li->prev->next = li->cur->next;
        free(li->cur);
        li->cur = li->prev->next;
    }
}


void deleteList(SllistType *li)
{
    if (li->root == NULL) return;
    SllistNode *temp;
    temp = li->root;
    while (temp != NULL)
    {
      li->root = li->root->next;
      free(temp);
      temp = li->root;
    }
}

void deleteAtPosition(SllistType* li, int n)
{
  int i = 0;
  SllistNode *prev2;
  li->prev = NULL;
  for (li->cur=li->root; li->cur != NULL; li->cur=li->cur->next)
    {
      if (i == n) break;
      i++;
      prev2 = li->prev;
      li->prev = li->cur;
    }
  if (li->cur == NULL)
    {
      li->cur = li->prev;
      li->prev = prev2;
      printf("Loi, khong co phan tu thu %d trong danh sach\n",n);
      return NULL;
    }
  deleteCur(li);
}


SllistNode *searchFunc(SllistType *li,elementtype element)
{
    SllistNode *prev2, *temp;
    li->cur = li->root;
    li->prev = NULL;
    while (li->cur != NULL)
    {
      if(func(li->cur->element,element)==0) break;
      prev2=li->prev;
      li->prev = li->cur;
      li->cur = li->cur->next;
    }
    temp = li->cur;
    if (li->cur == NULL)
    {
      li->cur = li->prev;
      li->prev = prev2;
    }
    return temp;
}

#include <stdio.h>
#include <stdlib.h>

//test list don ver 2

typedef struct
{
    char hoten[100];
    int mssv;
}elementtype;

#include "../sllib_ver2.h"

void printfSllistNode(elementtype e)
{
    printf("%s - %d\n",e.hoten,e.mssv);
}
int func(elementtype e1, elementtype e2)
{
    if(e1.mssv==e2.mssv) return 0;
    else return -1;
}

int sosanhSllist(SllistNode* node1,SllistNode* node2)
{
    return strcmp(node1->element.hoten,node2->element.hoten);
}
SllistType *li;

elementtype scanfElement();

int main()
{
    int end = 18;
    int i,j,n;
    int ch;
    li = makeSllist();
    elementtype* e;
    SllistNode * node = NULL;
    while(ch!=end)
    {
        printf("\nMenu test Single Link List\n\n");
        printf("1. Traverse List\n");
        printf("2. Insert Affter\n");
        printf("3. Insert Before\n");
        printf("4. Insert At Beginning\n");
        printf("5. Insert At End\n");
        printf("6. Insert At Position\n");
        printf("7. Delete At Beginning\n");
        printf("8. Delete At End\n");
        printf("9. Delete Node\n");
        printf("10.Delete List\n");
        printf("11.Delete At Position\n");
        printf("12.Search Func\n");
        printf("13.Number Node\n");
        printf("14.Update Node\n");
        printf("15.Sort\n");
        printf("16.Ton tai vi tri?\n");
        printf("17.Tim Node tai vi tri\n");
        printf("%d. Thoat\n\n",end);
        printf("Nhap lua chon: ");
        scanf("%d%*c",&ch);
        while(ch<1 && ch>end)
        {
            printf("Khong co lua chon nao. Nhap lai: ");
            scanf("%d%*c",&ch);
        }

        switch(ch)
        {
            case 1:
                traverseSllist(li);
                break;
            case 2:
                printf("Nhap ma so sinh vien can tim kiem\n");
                if((node=searchFunc(li,scanfElement()))!=NULL)
                {
                    printf("Tim thay: ");
                    printfSllistNode(node->element);
                }

                else
                    printf("Khong tim thay\n");
                while((ch=getchar())!='\n');
                if(node!=NULL) insertAfterNodeSllist(li,scanfElement(),node);
                break;
            case 3:
                printf("Nhap ma so sinh vien can tim kiem\n");
                if((node=searchFunc(li,scanfElement()))!=NULL)
                {
                    printf("Tim thay: ");
                    printfSllistNode(node->element);
                }

                else
                    printf("Khong tim thay\n");
                while((ch=getchar())!='\n');
                if(node!=NULL) insertBeforeNodeSllist(li,scanfElement(),node);
                break;
            case 4:
                insertAtBeginningSllist(li,scanfElement());
                break;
            case 5:
                insertAtEndSllist(li,scanfElement());
                break;
            case 6:
                printf("Nhap vi tri can chen: ");
                scanf("%d%*c",&j);
                insertAtPositionSlllist(li,scanfElement(),j);
                break;
            case 7:
                deleteAtBeginning(li);
                break;
            case 8:
                deleteAtEnd(li);
                break;
            case 9:
                printf("Nhap ma so sinh vien can tim kiem\n");
                if((node=searchFunc(li,scanfElement()))!=NULL)
                {
                    printf("Tim thay: ");
                    printfSllistNode(node->element);
                }

                else
                    printf("Khong tim thay\n");
                while((ch=getchar())!='\n');
                deleteNode(li,node);
                break;
            case 10:
                deleteList(li);
                break;
            case 11:
                printf("Nhap vi tri can xoa: ");
                scanf("%d%*c",&j);
                deleteAtPosition(li,j);
                break;
            case 12:
                printf("Nhap ma so sinh vien can tim kiem\n");
                if((node=searchFunc(li,scanfElement()))!=NULL)
                {
                    printf("Tim thay: ");
                    printfSllistNode(node->element);
                }

                else
                    printf("Khong tim thay\n");
                break;
            case 13:
                printf("So Node: %d\n",numberNodeSllist(li));
                break;
            case 14:
                printf("Nhap ma so sinh vien can tim kiem\n");
                if((node=searchFunc(li,scanfElement()))!=NULL)
                {
                    printf("Tim thay: ");
                    printfSllistNode(node->element);
                    printf("Nhap thong tin update\n");
                    updateSllist(node,scanfElement());
                    printf("Update thanh cong\n");
                }
                else
                    printf("Khong tim thay\n");
            case 15:
                quicksortSllist(li);
                break;
            case 16:
                printf("Nhap vi tri: ");
                scanf("%d%*c",&i);
                if(hasPositionSllist(li,i)) printf("Ton tai vi tri\n");
                else
                    printf("Khong ton tai vi tri\n");
                break;
            case 17:
                printf("Nhap vi tri: ");
                scanf("%d%*c",&i);
                if((node = findAtPositionSllist(li,i))!=NULL)
                {
                    printf("Tim thay\n");
                    printfSllistNode(node->element);
                }
                else
                    printf("Khong ton tai vi tri\n");
                break;
            case 18:  //sua o day/////////////////////////////
                return 0;
                break;
        }
    }

}

elementtype scanfElement()
{
    elementtype *e;
    e= (elementtype *)malloc(sizeof(elementtype));
    printf("Nhap ho ten: ");
    scanf("%[^\n]",e->hoten);
    printf("Nhap ma so sinh vien: ");
    scanf("%d%*c",&e->mssv);
    return *e;
}

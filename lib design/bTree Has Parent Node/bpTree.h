#include<stdio.h>
#include<conio.h>
#include<stdlib.h>
#include<stdbool.h>
#include<string.h>


typedef struct TreeNode TreeNode;

//dinh nghia ham insCompare, searchCompare,delCompare, printElement
struct TreeNode
{
    bTreeElementtype element;
    TreeNode* left;
    TreeNode* right;
    TreeNode* parent;
};

typedef TreeNode* TreeType;

void makeTree(TreeType *tree)
{
    (*tree) = NULL;
}

TreeType leftTree(TreeType root)
{
    if(root!=NULL)
        return root->left;
}

TreeType rightTree(TreeType root)
{
    if(root!=NULL)
        return root->right;
}

bool isLeaf(TreeNode* node)
{
    if(node!=NULL)
        return node->left == NULL && node->right == NULL;
    else return false;
}

bool isEmptyTree(TreeType root)
{
    return root == NULL;
}

TreeNode* makeTreeNode(bTreeElementtype e)
{
    TreeNode *node;
    node = (TreeNode *)malloc(sizeof(TreeNode));
    if(node!= NULL)
    {
        node->element = e;
        node->left = NULL;
        node->right = NULL;
        node->parent = NULL;
    }
    return node;
}

int numberNodeTree(TreeType root)
{
    if(root == NULL) return 0;
    return 1 + numberNodeTree(leftTree(root)) + numberNodeTree(rightTree(root));
}


int heightOfTree(TreeType root)
{
    if(root == NULL) return 0;
    int n1,n2;
    n1 = heightOfTree(leftTree(root));
    n2 = heightOfTree(rightTree(root));
    if(n1>=n2) return 1 + n1;
    else return 1 + n2;
}

int numberOfLeaf(TreeType root)
{
    if(root == NULL) return 0;
    if(isLeaf(root)) return 1;
    return numberOfLeaf(leftTree(root)) + numberOfLeaf(rightTree(root));
}

int numberOfInternalNode(TreeType root)
{
    if(root == NULL || isLeaf(root)) return 0;
    return 1 + numberOfInternalNode(leftTree(root)) + numberOfInternalNode(rightTree(root));
}

TreeType creatFrom2(TreeType l,TreeType r,bTreeElementtype e)
{
    TreeType root;
    root = (TreeNode*)malloc(sizeof(TreeNode));
    if(root!=NULL)
    {
        root->element = e;
        root->left = l;
        root->right = r;
        root->parent = NULL;
    }
    return root;
}

// add node vào trái nhất, trả về nốt được add
TreeNode* addLeft(TreeType *root,bTreeElementtype e)
{
    TreeNode* node = makeTreeNode(e);
    if(node==NULL) return NULL;
    if(*root==NULL) *root = node;
    else
    {
        TreeNode* LNode = *root;
        while(LNode->left != NULL)
        LNode = LNode->left;
        LNode->left = node;
        node->parent = LNode;
    }
    return node;
}

// add node vào phải nhất, trả về nốt được add
TreeNode* addRight(TreeType *root,bTreeElementtype e)
{
    TreeNode* node = makeTreeNode(e);
    if(node==NULL) return node;
    if(*root==NULL) *root = node;
    else
    {
        TreeNode* RNode = *root;
        while(RNode->right != NULL)
        RNode = RNode->right;
        RNode->right = node;
        node->right = RNode;
    }
    return node;
}

int numberOfRightChildren(TreeType root)
{
    if (root == NULL) return 0;
    if (rightTree(root) != NULL && isLeaf(rightTree(root))) return 1 + numberOfRightChildren(leftTree(root));
    if (leftTree(root) != NULL) return numberOfRightChildren(leftTree(root)) + numberOfRightChildren(rightTree(root)) + 1;
    else return numberOfRightChildren(leftTree(root));
}


void bInsert(TreeType *root,bTreeElementtype e)
{
    TreeNode* node;
    node = makeTreeNode(e);
    if((*root) == NULL)
    {
        *root = node;
        return;
    }else
    {
        if((*root)->left == NULL && insCompare((*root)->element,e)>=0 )
        {
            (*root)->left = node;
            node->parent = *root;
        }else
        if((*root)->right == NULL && insCompare((*root)->element,e)<0 )
        {
            (*root)->right = node;
            node->parent = *root;
        }else
        if(insCompare((*root)->element,e)>=0)
            bInsert(&((*root)->left),e);
        else
        if(insCompare((*root)->element,e)<0)
            bInsert(&((*root)->right),e);
    }
}

//in theo thứ tự tăng dần
void inOrderPrint(TreeType root)
{
  if (root == NULL) return;
  inOrderPrint(root->left);
  printElement(root->element);
  inOrderPrint(root->right);
}

//in theo thứ tự giảm dần
void preOrderPrint(TreeType root)
{
  if (root == NULL) return;
  preOrderPrint(root->right);
  printElement(root->element);
  preOrderPrint(root->left);
}

TreeNode *bSearch(TreeType root,bTreeElementtype e)
{
  if (root == NULL) return NULL;
  if(searchCompare(root->element,e)==0) return root;
  if(bSearch(root->left,e)!=NULL) return bSearch(root->left,e);
  if(bSearch(root->right,e)!=NULL) return bSearch(root->right,e);
  return NULL;
}

//tìm trái nhất,key nhỏ nhất
TreeNode *findMin(TreeType root)
{
  if (root == NULL) return NULL;
  while (root->left != NULL) root = root->left;
  return root;
}

//tìm phải nhất,key lớn nhất
TreeNode *findMax(TreeType root)
{
  if (root == NULL) return NULL;
  while (root->right != NULL) root = root->right;
  return root;
}


TreeNode* DeleteLeaf(TreeType *root,TreeNode* z)
{
    if(z == NULL) return NULL;
    if(z==(*root))
    {
        free(z);
        (*root) = NULL;
        return NULL;
    }
    TreeNode* parent = z->parent;
    if(parent!= NULL)
    {
        if(parent->left == z) parent->left = NULL;
        if(parent->right == z) parent->right = NULL;
    }
    free(z);
    return parent;
}

TreeNode* DeleteOnlyLeft(TreeType *root,TreeNode* z)
{
    if(z==NULL) return NULL;
    if(z==(*root))
    {
        (*root) = z->left;
        free(z);
        return NULL;
    }
    TreeNode* parent = z->parent;
    if(parent!=NULL)
    {
        if(parent->left == z) parent->left = z->left;
        if(parent->right == z) parent->right = z->left;
        z->left->parent = parent;
    }
    free(z);
    return parent;
}

TreeNode* DeleteOnlyRight(TreeType *root,TreeNode* z)
{
    if(z==NULL) return NULL;
    if(z==(*root))
    {
        (*root) = z->left;
        free(z);
        return NULL;
    }
    TreeNode* parent = z->parent;
    if(parent!=NULL)
    {
        if(parent->left == z) parent->left = z->right;
        if(parent->right == z) parent->right = z->right;
        z->right->parent = parent;
    }
    free(z);
    return parent;
}

TreeNode* DeleteTwoChild(TreeType *root,TreeNode* z)
{
    if(z==NULL) return NULL;
    TreeNode* parent = z->parent;
    TreeNode* tmp = findMin(z->right);
    z->element = tmp->element;
    if(tmp->right == NULL) DeleteLeaf(root,tmp);
    else DeleteOnlyRight(root,tmp);
    return parent;
}

TreeNode* deleteNode(TreeType* root,bTreeElementtype e)
{
    TreeNode* z;
    z = bSearch(*root,e);
    if(z == NULL) return NULL;
    if(z->left == NULL && z->right == NULL) return DeleteLeaf(root,z);
    if(z->left != NULL && z->right == NULL) return DeleteOnlyLeft(root,z);
    if(z->right != NULL && z->left == NULL) return DeleteOnlyRight(root,z);
    if(z->left != NULL && z->right != NULL) return DeleteTwoChild(root,z);
}

TreeNode* deleteNode1(TreeType* root,TreeNode* z)
{
    if(z == NULL) return NULL;
    if(z->left == NULL && z->right == NULL) return DeleteLeaf(root,z);
    if(z->left != NULL && z->right == NULL) return DeleteOnlyLeft(root,z);
    if(z->right != NULL && z->left == NULL) return DeleteOnlyRight(root,z);
    if(z->left != NULL && z->right != NULL) return DeleteTwoChild(root,z);
}

void deleteTree(TreeType* root)
{
    while((*root) !=NULL)
        deleteNode(root,(*root)->element);
}




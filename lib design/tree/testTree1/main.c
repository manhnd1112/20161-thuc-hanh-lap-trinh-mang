#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
#include"..\tree1.h"

int main()
{
    int ch,i,n,max,min,rightmostvalue;
    int end=9;
    BinaryTree *tree;
    tree = CreateBinaryTree();
    while(ch!=end)
    {
        printf("1. IsEmpty\n");
        printf("2. IsLeaf\n");
        printf("3. Add\n");
        printf("4. Max\n");
        printf("5. Min\n");
        printf("6. Rightmost\n");
        printf("7. Delete RightMost\n");
        printf("8. Duyet LRM\n");
        printf("9. Thoat\n");
        printf("Nhap lua chon: ");
        scanf("%d%*c",&ch);
        while(ch<1 || ch>9)
        {
            printf("Khong co su lua chon nao. Nhap lai: ");
            scanf("%d%*c",&ch);
        }
        switch(ch)
        {
            case 1:
                if(IsEmpty(tree)) printf("Tree is Empty\n\n");
                else printf("Tree is not Empty\n\n");
                break;
            case 2:
                break;
            case 3:
                printf("Nhap so: ");
                scanf("%d%*c",&n);

                Add(tree,n);
                break;
            case 4:
                if(Max(tree,&max)) printf("Max: %d\n",max);
                else
                    printf("Tree is Empty\n");
                break;
            case 5:
                if(Min(tree,&min)) printf("Min: %d\n",min);
                else
                    printf("Tree is Empty\n");
                break;
            case 6:
                if(RightMost(tree,&rightmostvalue)) printf("RightMost value: %d\n",rightmostvalue);
                else printf("Tree is Empty\n");
                break;
            case 7:
                DeleteRightMost(tree);
                break;
            case 8:
                inorder(tree);
                printf("\n");
                break;

            case 9:
                getch();
                return 0;
        }
    }
}


/*
task: please implement all the listed APIs below in a file
named tree.c
*/


#include <stdbool.h>

// stdbool.h header is declared to use bool type.

typedef struct BinaryNode {
  int data;
  struct BinaryNode* left;
  struct BinaryNode* right;
} BinaryNode;

typedef struct BinaryTree {
  int depth;  // Hold tree depth
  int size;  // Hold number of tree node
  BinaryNode* root;
} BinaryTree;

// Return a pointer to an empty tree struct
// depth = size = 0; root = NULL;
BinaryTree* CreateBinaryTree();

// return true if node is a leaf, false otherwise
bool IsLeaf(BinaryNode* node);

// return true, if tree is empty
bool IsEmpty(const BinaryTree* tree);

// Add a new node, contains data in the position is the right
// most of the highest level. Please see example below:
// if we add 1 2 3 4 5 6, the tree should be grown up as:
//   1      1      1            1            1            1
//        2      2   3        2   3        2   3        2   3
//                         4             4   5        4   5   6
// And to make sure that depth and size of the tree struct
// contain correct values
void Add(BinaryTree* tree, int data);

// return true if success, false if tree is empty
// *data contains the max value in the tree
bool Max(const BinaryTree* tree, int* data);

// return true if success, false if tree is empty
// *data contains the min value in the tree
bool Min(const BinaryTree* tree, int* data);

// Get the right most element of tree, return true if success
// false if tree is empty
bool RightMost(const BinaryTree* tree, int* data);

// Delete the right most node on the deapest level of tree
// Empty the root pointer of tree if no element is left
void DeleteRightMost(BinaryTree* tree);

BinaryTree* CreateBinaryTree()
{
    BinaryTree* tree = (BinaryTree *)malloc(sizeof(BinaryTree));
    if(tree==NULL) return NULL;
    tree->size  = 0;
    tree->depth = 0;
    tree->root = NULL;
    return tree;
}

bool IsLeaf(BinaryNode* node)
{
    if(node!=NULL)
        return node->left == NULL && node->right == NULL;
    else return false;
}

bool IsEmpty(const BinaryTree* tree)
{
    return tree->root == NULL;
}

BinaryNode* findNodeAtPosition(BinaryNode *root,int n,int *i)
{
    int l = 2*(*i);
    int r = 2*(*i) +1;
    if(root==NULL) return NULL;
    if(*i==n) return root;
    if(root->left==NULL && root->right==NULL) return NULL;

    if(findNodeAtPosition(root->left,n,&l) == NULL)
    return findNodeAtPosition(root->right,n,&r);

    if(findNodeAtPosition(root->right,n,&r) == NULL)
    return findNodeAtPosition(root->left,n,&l);
}

void Add(BinaryTree* tree, int data)
{
    int n = 0;
    int p = 0;
    int i = 0;
    BinaryNode *node;
    BinaryNode *newNode;
    if(tree->root==NULL)
    {
        tree->root = (BinaryNode *)malloc(sizeof(BinaryNode));
        tree->root->data = data;
        tree->root->left = NULL;
        tree->root->right = NULL;
        tree->size = 1;
        tree->depth = 1;
    }else
    {
        n = tree->size;
        n = n+1;
        p = n/2;
        i = 1;
        node = findNodeAtPosition(tree->root,p,&i);
        if(node!=NULL)
        {
            newNode = (BinaryNode *)malloc(sizeof(BinaryNode));
            newNode->data = data;
            newNode->left = NULL;
            newNode->right = NULL;
            if(node->left == NULL)
            {
                printf("chen vao trai sau node: %d\n",node->data);
                node->left = newNode;
            }else
            {
                printf("chen vao phai sau node: %d\n",node->data);
                node->right = newNode;
            }

            tree->size++;
            n = tree->size;
            i = 0;
            while(n!=0)
            {
                i++;
                n = n/2;
            }
            tree->depth = i;
        }
    }
}

bool Max(const BinaryTree* tree, int* data)
{
    BinaryNode* node;
    if(IsEmpty(tree)) return false;
    *data = tree->root->data;
    Max1(tree->root,data);
    return true;
}

void Max1(BinaryNode* root,int* data)
{
    if(root!= NULL)
    {
        if(*data < root->data) *data= root->data;
        if(root->left != NULL) Max1(root->left,data);
        if(root->right != NULL) Max1(root->right,data);
    }
}

bool Min(const BinaryTree* tree, int* data)
{
    BinaryNode* node;
    if(IsEmpty(tree)) return false;
    *data = tree->root->data;
    Min1(tree->root,data);
    return true;
}

void Min1(BinaryNode* root,int* data)
{
    if(root!=NULL)
    {
        if(*data > root->data) *data = root->data;
        if(root->left != NULL) Min1(root->left,data);
        if(root->right != NULL) Min1(root->right,data);
    }
}

bool RightMost(const BinaryTree* tree, int* data)
{
    BinaryTree* newTree;
    if(IsEmpty(tree)) return false;
    if(tree->root->right!=NULL)
    {
        if(tree->root->right->right == NULL)
        {
            *data = tree->root->right->data;
            return true;
        }
        else
        {
            newTree = (BinaryTree *)malloc(sizeof(BinaryTree));
            newTree->root = tree->root->right;
            return RightMost(newTree,data);
        }
    }else
    if(tree->root->left == NULL)
    {
        *data = tree->root->data;
        return true;
    }else
    {
        newTree = (BinaryTree *)malloc(sizeof(BinaryTree));
        newTree->root = tree->root->left;
        return RightMost(newTree,data);
    }
}

BinaryNode* getRightMostNode(BinaryTree* tree)
{
    BinaryTree* newTree;
    if(IsEmpty(tree)) return NULL;
    if(tree->root->right!=NULL)
    {
        if(tree->root->right->right == NULL)
        {
            return tree->root->right;
        }
        else
        {
            newTree = (BinaryTree *)malloc(sizeof(BinaryTree));
            newTree->root = tree->root->right;
            return getRightMostNode(newTree);
        }
    }else
    if(tree->root->left == NULL)
    {
        return tree->root->data;
    }else
    {
        newTree = (BinaryTree *)malloc(sizeof(BinaryTree));
        newTree->root = tree->root->left;
        return getRightMostNode(newTree);
    }
}

void DeleteRightMost(BinaryTree *tree)
{
    int i,n;
    BinaryNode* node;
    BinaryNode* RightMostNode;
    node = (BinaryNode *)malloc(sizeof(BinaryNode));
    RightMostNode = (BinaryNode *)malloc(sizeof(BinaryNode));
    if(tree->root == NULL) return;
    RightMostNode = getRightMostNode(tree);
    for(i = 1;i<=tree->size;i++)
    {
        n = 1;
        node = findNodeAtPosition(tree->root,i,&n);
        if(node == RightMostNode) break;
    }
    n= 1;
    node = findNodeAtPosition(tree->root,i/2,&n);
    if(node==NULL)
    {
        printf("fjdlakjflaj\n");
        if(tree->root->left!=NULL) tree->root = tree->root->left;
        else
            tree->root = NULL;
    }else
    {
        node->right = NULL;
        printf("jfdlkajflajf\n");
    }

}

//tang dan
void inorder(BinaryTree* tree)
{
    BinaryTree* LeftTree, *RightTree;
    if(tree->root == NULL) return;
    if(tree->root->left!=NULL)
    {
        LeftTree = (BinaryTree *)malloc(sizeof(BinaryTree));
        LeftTree->root = tree->root->left;
        inorder(LeftTree);
    }
    printf("%d, ",tree->root->data);
    if(tree->root->right!=NULL)
    {
        RightTree = (BinaryTree *)malloc(sizeof(BinaryTree));
        RightTree->root = tree->root->right;
        inorder(RightTree);
    }
}

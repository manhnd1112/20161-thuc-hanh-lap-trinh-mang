#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"bst.h"
void DocFile(char *filename);
void SaveFile(BSTree* root,FILE *fptr);
BSTree* tree;
int main(int argc,char* argv[])
{
    FILE *fptr1;
    FILE *fptr2;
    FILE *fptr3;
    elementtype *e;
    if(argc!=3 && argc!=5)
    {
        printf("Doi so dong lenh sai\n");
    }else
    if(strcmp(argv[1],"--merge")==0)
    {
        tree = NULL;
        fptr3 = fopen(argv[4],"w");
        if(fptr3==NULL)
        {
            return 1;
        }
        DocFile(argv[2]);
        DocFile(argv[3]);
        SaveFile(tree,fptr3);

    }else
    if(strcmp(argv[1],"--add")==0)
    {
        tree = NULL;
        DocFile(argv[2]);
        e = (elementtype *)malloc(sizeof(elementtype));
        printf("Nhap mssv: ");
        scanf("%[^\n]%*c",e->mssv);
        printf("Nhap hvt: ");
        scanf("%[^\n]%*c",e->name);
        printf("Nhap ns: ");
        scanf("%d%*c",&e->ns);
        if(Search(tree,*e)==NULL)
        {
            fptr1 = fopen(argv[2],"w");
            if(fptr1==NULL) return;
            Insert(*e,tree);
            SaveFile(tree,fptr1);
        }
    }
    else
    if(strcmp(argv[1],"--sort")==0)
    {
        tree==NULL;
        DocFile(argv[2]);
        fptr1 = fopen(argv[2],"w");
        if(fptr1 ==NULL) return;
        SaveFile(tree,fptr1);
    }
    return 0;
}

void DocFile(char *filename)
{
    FILE *fptr;
    char s[80],str[80],*str1,*str4;
    char str2[60],str3[2];
    int i,j=0;
    elementtype *e;
    fptr = fopen(filename,"r");
    if(fptr == NULL) return;
    while(fgets(s,80,fptr)!= NULL)
    {
        if(strstr(s,"{")!=NULL)
        {
            e = (elementtype* )malloc(sizeof(elementtype));
            for(i=0;i<3;i++)
            {
                fscanf(fptr,"%[^\n]\n",str);
                if((str1=strstr(str,"mssv: "))!=NULL)
                {
                    strcpy(str2,strstr(str1," "));
                    str3[0]=str2[1];
                    str3[1] = '\0';
                    str4 = strstr(str2,str3);
                    strcpy(e->mssv,str4);
                }
                if((str1=strstr(str,"hvt: "))!=NULL)
                {
                    strcpy(str2,strstr(str1," "));
                    str3[0]=str2[1];
                    str3[1] = '\0';
                    str4 = strstr(str2,str3);
                    strcpy(e->name,str4);
                }
                if((str1=strstr(str,"ns: "))!=NULL)
                {
                    str4 = strstr(str1," ");
                    e->ns = atoi(str4);
                }
            }
            if(tree==NULL)
            {
                tree = (BSTree *)malloc(sizeof(BSTree));
                tree->data = *e;
                tree->parent = NULL;
                tree->left = NULL;
                tree->right = NULL;
            }else
            {
                if(Search(tree,*e)==NULL)
                    Insert(*e,tree);
            }
            fgets(s,80,fptr); // doc dau "}"
        }
    }
}

void SaveFile(BSTree* root,FILE *fptr)
{
    if(root==NULL) return;
    SaveFile(root->left,fptr);
    fprintf(fptr,"{\n");
    //printf("mssv: %s\n",root->data.mssv);
    fprintf(fptr,"mssv: %s\n",root->data.mssv);
    fprintf(fptr,"hvt: %s\n",root->data.name);
    fprintf(fptr,"ns: %d\n",root->data.ns);
    fprintf(fptr,"}\n");
    SaveFile(root->right,fptr);
}

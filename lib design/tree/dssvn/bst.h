
/*
C Basic 17/11/2015
task: please implement all the listed APIs below in a file
named bintree.c
*/
#include<stdio.h>
#include<stdlib.h>
typedef struct{
    char name[50];
    char mssv[10];
    int ns;
}elementtype;

typedef struct BSTree {
  elementtype data;
  struct BSTree* parent;
  struct BSTree* left;
  struct BSTree* right;
} BSTree;

// In cây theo trật tự Preorder
//void Print(BSTree* tree, WalkMethod walk_method);

void Print1(BSTree* tree, int walk_method);

// Trả về con trỏ tới cây có nút gốc = x
//BSTree* Search(int x, BSTree* tree);

// Trả về con trỏ tới cây có nút gốc nhỏ nhất
//BSTree* TreeMin(BSTree* tree);

// Trả về con trỏ có nút gốc cực đại
//BSTree* TreeMax(BSTree* tree);

// Trả về nút liền sau của nút z trong tree
//BSTree* Successor(BSTree* z);

// Đảm bảo trái <= gốc & gốc <= phải
void Insert(elementtype data, BSTree* tree);

// Xóa nút z trong cây tree
//BSTree* Delete(BSTree* z);

void Print1(BSTree* tree, int walk_method)
{
    if(walk_method == 0)
    {
        if(tree==NULL) return;
        Print1(tree->left,walk_method);
        printf("{\n");
        printf("hvt: %s\n",tree->data.name);
        printf("mssv: %s\n",tree->data.mssv);
        printf("ns: %d\n",tree->data.ns);
        printf("}\n");
        Print1(tree->right,walk_method);
    }
    if(walk_method == 1)
    {
        if(tree==NULL) return;
        printf("{\n");
        printf("hvt: %s\n",tree->data.name);
        printf("mssv: %s\n",tree->data.mssv);
        printf("ns: %d\n",tree->data.ns);
        printf("}\n");
        Print1(tree->left,walk_method);
        Print1(tree->right,walk_method);
    }
    if(walk_method == 2)
    {
        if(tree==NULL) return;
        Print1(tree->left,walk_method);
        Print1(tree->right,walk_method);
        printf("{\n");
        printf("hvt: %s\n",tree->data.name);
        printf("mssv: %s\n",tree->data.mssv);
        printf("ns: %d\n",tree->data.ns);
        printf("}\n");
    }
}

void Insert(elementtype data,BSTree* tree)
{
    BSTree* node;
    if(tree == NULL)
    {
        return;
    }else
    {
        node = (BSTree*)malloc(sizeof(BSTree));
        node->data = data;
        node->left = NULL;
        node->right = NULL;
        if(tree->left == NULL && tree->data.ns > data.ns)
        {
            tree->left = node;
            node->parent = tree;
        }else
        if(tree->right == NULL && tree->data.ns <= data.ns)
        {
            tree->right = node;
            node->parent = tree;
        }else
        if(tree->data.ns > data.ns)
            Insert(data,tree->left);
        else
        if(tree->data.ns <= data.ns)
            Insert(data,tree->right);
    }
}

BSTree* Search(BSTree* tree, elementtype e)
{
    if(tree==NULL) return;
    if(strcmp(tree->data.mssv,e.mssv)==0) return tree;
    Search(tree->left,e);
    Search(tree->right,e);
}






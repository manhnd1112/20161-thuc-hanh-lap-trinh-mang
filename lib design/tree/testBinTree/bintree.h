/*
C Basic 17/11/2015
task: please implement all the listed APIs below in a file
named bintree.c
*/

typedef enum WalkMethod {
  IN_ORDER = 0,  // trái, giá trị, phải
  PRE_ORDER,  // giá trị, trái, phải
  POST_ORDER,  // trái, phải, giá trị
  MAX_WALK_METHOD
};

typedef struct BSTree {
  int data;
  struct BSTree* parent;
  struct BSTree* left;
  struct BSTree* right;
} BSTree;

// In cây theo trật tự Preorder
void Print(BSTree* tree, WalkMethod walk_method);

// Trả về con trỏ tới cây có nút gốc = x
BSTree* Search(int x, BSTree* tree);

// Trả về con trỏ tới cây có nút gốc nhỏ nhất
BSTree* TreeMin(BSTree* tree);

// Trả về con trỏ có nút gốc cực đại
BSTree* TreeMax(BSTree* tree);

// Trả về nút liền sau của nút z trong tree
BSTree* Successor(BSTree* z);

// Đảm bảo trái <= gốc & gốc <= phải
void Insert(int data, BSTree* tree);

// Xóa nút z trong cây tree
BSTree* Delete(BSTree* z);

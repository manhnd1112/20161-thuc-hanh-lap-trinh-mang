#include<stdio.h>
#include<stdlib.h>
#include "../bintree.c"
int main()
{
    int ch = -1,i;
    BSTree *node,*tree;
    tree = (BSTree*)malloc(sizeof(tree));
    tree->data = 9;
    tree->parent = NULL;
    tree->left = NULL;
    tree->right = NULL;
    while(ch!=8)
    {
        printf("1- Print\n");
        printf("2- Search\n");
        printf("3- TreeMin\n");
        printf("4- TreeMax\n");
        printf("5- Successor\n");
        printf("6- Insert\n");
        printf("7- Delete\n");
        printf("8- Exit\n");
        printf("Nhap lua chon: ");
        scanf("%d%*c",&ch);
        while(ch<1 && ch>8)
        {
            printf("Khong co lua chon nao. Nhap lai: ");
            scanf("%d%*c",&ch);
        }
        switch(ch)
        {
            case 1:
                Print(tree,0);
                printf("\n");
                break;
            case 2:
                printf("Nhap gia tri can tim: ");
                scanf("%d%*c",&i);
                node = Search(i,tree);
                if(node!= NULL)
                {
                    printf("Tim thay\n");
                    if(node->parent!= NULL)
                        printf("parent Node: %d\n",node->parent->data);
                    else printf("parent Node: NULL\n");
                    if(node->left!= NULL)
                        printf("left Node: %d\n",node->left->data);
                    else printf("left Node: NULL\n");
                    if(node->right!= NULL)
                        printf("right Node: %d\n",node->right->data);
                    else printf("right Node: NULL\n");
                }else
                printf("Khong tim thay\n");
                break;
            case 3:
                if(TreeMin(tree)!=NULL)
                printf("TreeMin: %d\n",TreeMin(tree)->data);
                else printf("NULL\n");
                break;
            case 4:
                if(TreeMin(tree)!=NULL)
                printf("TreeMax: %d\n",TreeMax(tree)->data);
                else printf("NULL\n");
                break;
            case 5:
                printf("Nhap gia tri can tim successor: ");
                scanf("%d%*c",&i);
                if(Successor(Search(i,tree))!=NULL)
                    printf("Successor: %d\n",Successor(Search(i,tree))->data);
                else printf("Successor is NULL\n");
                break;
            case 6:
                printf("Nhap gia tri can insert: ");
                scanf("%d%*c",&i);
                Insert(i,tree);
                break;
            case 7:
                printf("Nhap gia tri can xoa: ");
                scanf("%d%*c",&i);
                node = Delete(Search(i,tree));
                if(node==NULL) printf("Parent of Deleted Node is NULL\n");
                else
                    printf("Parent of Deleted Node is: %d\n",node->data);
                break;
            case 8:
                return 0;
                break;
        }
    }
}

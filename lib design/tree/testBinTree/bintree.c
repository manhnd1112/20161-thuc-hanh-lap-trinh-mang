#include<stdio.h>
#include<stdlib.h>
#include"bintree.h"

void Print(BSTree* tree, WalkMethod walk_method)
{
    if(walk_method == IN_ORDER)
    {
        if(tree==NULL) return;
        Print(tree->left,IN_ORDER);
        Print("%d, ",tree->data);
        Print(tree->right,IN_ORDER);
    }
    if(walk_method == PRE_ORDER)
    {
        if(tree==NULL) return;
        Print("%d, ",tree->data);
        Print(tree->left,PRE_ORDER);
        Print(tree->right,PRE_ORDER);
    }
    if(walk_method == POST_ORDER)
    {
        if(tree==NULL) return;
        Print(tree->left,POST_ORDER);
        Print(tree->right,POST_ORDER);
        Print("%d, ",tree->data);
    }
}

BSTree* Search(int x, BSTree* tree)
{
    BSTree* node;
    if(tree == NULL) return NULL;
    if(x == tree->data) return tree;
    if(x < tree->data)
    {
        return Search(x,tree->left);
    }
    if(x >= tree->data)
    {
        Search(x,tree->right);
    }
}

BSTree* TreeMin(BSTree* tree)
{
    if(tree==NULL) return NULL;
    if(tree->left == NULL) return tree;
    else
        return TreeMin(tree->left);
}

BSTree* TreeMax(BSTree* tree)
{
    if(tree==NULL) return NULL;
    if(tree->right == NULL) return tree;
    else
        return TreeMax(tree->right);
}

BSTree* Successor(BSTree* z)
{
    BSTree* node, *childNode;
    if(z == NULL) return NULL;
    if(z->right!= NULL) return TreeMin(z->right);
    else
    {
        if(z->parent!= NULL)
        {
            if(z->parent->left!= NULL && z->parent->left == z) return z->parent;
            else
            {
                childNode = z->parent;
                node = childNode->parent;
                while(node!= NULL)
                {
                    if(node->left == childNode) return node;
                    childNode = node;
                    node = childNode->parent;
                }
                return NULL;
            }
        } else return NULL;
    }
}


void Insert(int data,BSTree* tree)
{
    BSTree* node;
    if(tree == NULL)
    {
        return;
    }else
    {
    node = (BSTree*)malloc(sizeof(BSTree));
    node->data = data;
    node->left = NULL;
    node->right = NULL;
    if(tree->left == NULL && tree->data > data)
    {
        tree->left = node;
        node->parent = tree;
    }else
    if(tree->right == NULL && tree->data <= data)
    {
        tree->right = node;
        node->parent = tree;
    }else
    if(tree->data > data)
        Insert(data,tree->left);
    else
    if(tree->data <= data)
        Insert(data,tree->right);
    }
}


BSTree* DeleteLeaf(BSTree* z)
{
    if(z == NULL) return NULL;
    BSTree* parent = z->parent;
    if(parent!= NULL)
    {
        if(parent->left == z) parent->left = NULL;
        if(parent->right == z) parent->right = NULL;
    }
    free(z);
    return parent;
}

BSTree* DeleteOnlyLeft(BSTree* z)
{
    if(z==NULL) return NULL;
    BSTree* parent = z->parent;
    if(parent->left == z) parent->left = z->left;
    if(parent->right == z) parent->right = z->left;
    free(z);
    return parent;
}

BSTree* DeleteOnlyRight(BSTree* z)
{
    if(z==NULL) return NULL;
    BSTree* parent = z->parent;
    if(parent->left == z) parent->left = z->right;
    if(parent->right == z) parent->right = z->right;
    free(z);
    return parent;
}

BSTree* DeleteTwoChild(BSTree* z)
{
    if(z==NULL) return NULL;
    BSTree* parent = z->parent;
    BSTree* tmp = TreeMin(z->right);
    z->data = tmp->data;
    if(tmp->left == NULL && tmp->right == NULL) DeleteLeaf(tmp);
    if(tmp->right) DeleteOnlyRight(tmp);
    return parent;
}

BSTree* Delete(BSTree* z)
{
    if(z == NULL) return;
    if(z->left == NULL && z->right == NULL) return DeleteLeaf(z);
    if(z->left != NULL && z->right == NULL) return DeleteOnlyLeft(z);
    if(z->right != NULL && z->left == NULL) return DeleteOnlyRight(z);
    if(z->left != NULL && z->right != NULL) return DeleteTwoChild(z);
}

void Print1(BSTree* tree, int walk_method)
{
    if(walk_method == 0)
    {
        if(tree==NULL) return;
        Print1(tree->left,walk_method);
        printf("%d, ",tree->data);
        Print1(tree->right,walk_method);
    }
    if(walk_method == 1)
    {
        if(tree==NULL) return;
        printf("%d, ",tree->data);
        Print1(tree->left,walk_method);
        Print1(tree->right,walk_method);
    }
    if(walk_method == 2)
    {
        if(tree==NULL) return;
        Print1(tree->left,walk_method);
        Print1(tree->right,walk_method);
        printf("%d, ",tree->data);
    }
}

#include<stdio.h>
#include<conio.h>
#include<stdlib.h>

typedef struct TreeNode TreeNode;

struct TreeNode
{
    TreeElementtype element;
    TreeNode* left;
    TreeNode* right;
};

typedef TreeNode* TreeType;

void MakeTree(TreeType *tree)
{
    (*tree) = NULL;
}

TreeType LeftTree(TreeType root)
{
    if(root!=NULL)
        return root->left;
}

TreeType RightTree(TreeType root)
{
    if(root!=NULL)
        return root->right;
}

int isLeaf(TreeNode* node)
{
    if(node!=NULL)
        return node->left == NULL && node->right == NULL;
    else return 1;
}

int isEmptyTree(TreeType root)
{
    return root == NULL;
}

TreeNode* makeTreeNode(TreeElementtype e)
{
    TreeNode *node;
    node = (TreeNode *)malloc(sizeof(TreeNode));
    if(node!= NULL)
    {
        node->element = e;
        node->left = NULL;
        node->right = NULL;
    }
    return node;
}

int numberNodeTree(TreeType root)
{
    if(root == NULL)return 0;
    return 1 + numberNodeTree(LeftTree(root)) + numberNodeTree(RightTree(root));
}


int HeightOfTree(TreeType root)
{
    if(root == NULL) return 0;
    int n1,n2;
    n1 = HeightOfTree(LeftTree(root));
    n2 = HeightOfTree(RightTree(root));
    if(n1>=n2) return 1 + n1;
    else return 1 + n2;
}

int NumberOfLeaf(TreeType root)
{
    if(root == NULL) return 0;
    if(isLeaf(root)==0) return 1;
    return NumberOfLeaf(LeftTree(root)) + NumberOfLeaf(RightTree(root)) + 1;
}

int NumberOfInternalNode(TreeType root)
{
    if(root == NULL || isLeaf(root) == 0) return 0;
    return 1 + NumberOfInternalNode(LeftTree(root)) + NumberOfInternalNode(RightTree(root));
}

void DeleteTree(TreeType* root)
{
    TreeNode* temp;
    if(root == NULL) return;
    if(isLeaf(root)==0)
    {
        temp = root;
        free(temp);
        root = NULL;
        return;
    }
    DeleteTree(&LeftTree(root));
    DeleteTree(&RightTree(root));
}

TreeType creatFrom2(TreeType l,TreeType r,TreeElementtype e)
{
    TreeType root;
    root = (TreeNode*)malloc(sizeof(TreeNode));
    if(root!=NULL)
    {
        root->element = e;
        root->left = l;
        root->right = e;
    }
    return root;
}

TreeNode addLeft(TreeType *root,TreeElementtype e)
{
    TreeNode* node = makeTreeNode(e);
    if(node==NULL) return node;
    if(*root==NULL) *root = node;
    else
    {
        TreeNode* LNode = *root;
        while(LNode->left != NULL)
        LNode = LNode->left;
        LNode->left = node;
    }
    return node;
}

TreeNode addRight(TreeType *root,TreeElementtype e)
{
    TreeNode* node = makeTreeNode(e);
    if(node==NULL) return node;
    if(*root==NULL) *root = node;
    else
    {
        TreeNode* RNode = *root;
        while(RNode->right != NULL)
        RNode = RNode->right;
        RNode->right = node;
    }
    return node;
}

int numberOfRightChildren(TreeType root)
{
    if (root == NULL) return 0;
    if (RightTree(n) != NULL && isLeaf(RightTree(n))) return 1 + numberOfRightChildren(LeftTree(n));
    if (LeftTree(n) != NULL) return numberOfRightChildren(LeftTree(n)) + numberOfRightChildren(RightTree(n)) + 1;
    else return numberOfRightChildren(LeftTree(n));
}


#include"tree1.h"
#include<stdbool.h>
BinaryTree* CreateBinaryTree()
{
    BinaryTree* tree = (BinaryTree *)malloc(sizeof(BinaryTree));
    if(tree==NULL) return NULL;
    tree->size  = 0;
    tree->depth = 0;
    tree->root = NULL;
    return tree;
}

bool IsLeaf(BinaryNode* node)
{
    if(node!=NULL)
        return node->left == NULL && node->right == NULL;
    else return false;
}

bool IsEmpty(const BinaryTree* tree)
{
    return tree->root == NULL;
}

void Add(BinaryTree* tree, int data)
{
    int n = 0;
    int p = 0;
    int i = 0;
    BinaryNode *node;
    BinaryNode *newNode;
    if(tree->root==NULL)
    {
        tree->root->data = data;
        tree->root->left = NULL;
        tree->root->right = NULLL;
    }else
    {
        for(node = tree->root; node!= NULL; node = node->next)
        {
            n++;
        }
        n = n+1;
        p = n/2;
        for(node = tree->root;node!=NULL; node = node->next)
        {
            i++;
            if(i==p) break;
        }
        newNode->data = data;
        newNode->left = NULL;
        newNode->right = NULL;
        if(node->left!= NULL)
        {
            node->left = newNode;
        }else
            node->right = newNode;
    }
    tree->size++;
    n = tree->size;
    i = 0;
    while(n!=0)
    {
        i++;
        n = n/2;
    }
    tree->depth = i;
}

bool Max(const BinaryTree* tree, int* data)
{
    int maxvalue;
    BinaryNode* node;
    if(IsEmpty(tree)) return false;
    maxvalue = tree->root->data;
    for(node=tree->root; node!=NULL; node = node->next)
    {
        if(node->data > maxvalue)
            maxvalue = node->data;
    }
    *data = maxvalue;
    return true;
}

bool Min(const BinaryTree* tree, int* min)
{
    int minvalue;
    BinaryNode* node;
    if(IsEmpty(tree)) return false;
    minvalue = tree->root->data;
    for(node=tree->root; node!=NULL; node = node->next)
    {
        if(node->data > minvalue)
            minvalue = node->data;
    }
    *data = minvalue;
    return true;
}

bool RightMost(const BinaryTree* tree, int* data)
{
    BinaryTree* newTree;
    if(IsEmpty(tree)) return false;
    if(tree->root->right!=NULL)
    {
        if(tree->root->right->right == NULL)
        {
            *data = tree->root->right->data;
            return true;
        }
        else
        {
            newTree = (BinaryTree *)malloc(sizeof(BinaryTree));
            newTree->root = tree->root->right;
            return RightMost(newTree,data);
        }
    }else
    if(tree->root->left == NULL)
    {
        data = tree->root->data;
        return true;
    }else
    {
        newTree = (BinaryTree *)malloc(sizeof(BinaryTree));
        newTree->root = tree->root->left;
        return RightMost(newTree,data);
    }
}





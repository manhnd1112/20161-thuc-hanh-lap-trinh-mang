#include <stdio.h>

enum{
    SUBJECT_ID,
    SUBJECT,
    F,
    SEMESTER,
    STUDENT_COUNT,
    S
};

typedef struct{
    char id[9];
    char firstName[20];
    char lastName[10];
    float midTermPoint;
    float endTermPoint;
    char rank[2];
} elementtype;

#include "../lib design/Sllist/sllib_ver2.h"

typedef struct {
    char id[10];            // subjectID
    char name[50];          // subject
    int f1;                 // F - mid Term Point %
    int f2;                 // F - end Term Point %
    char semester[6];           // Semester
    int studentCount;       // Student Count
    SllistType *studentList; // Student List
} Subject;

int readData(Subject *sub);
int getPrefixVal(char *key);
char* getColumnValue(char *data, int index);
void printSubject(Subject *sub);
char* getRank(float midTermPoint, float endTermPoint, int f1, int f2);
float getMark(float midTermPoint, float endTermPoint, int f1, int f2);

int main(){
    Subject *sub;
    int ch = -1;
    char continue_flag;
    do{
        printf("Learning management system\n");
        printf("-----------------------------------------\n");
        printf("1.  Add a new score board\n");
        printf("2.  Add score\n");
        printf("3.  Remove score\n");
        printf("4.  Search score\n");
        printf("5.  Display score board and score report\n");
        printf("6.  Quit\n");
        printf("Enter your choose: ");
        scanf("%d%*c", &ch);
        switch(ch){
            case 1:
                do{
                    sub = (Subject *)malloc(sizeof(Subject));
                    sub->studentList = makeSllist();
                    printf("_____ Add a new score board ______\n");
                    printf("Enter the id of the subject: ");
                    scanf("%[^\n]%*c", sub->id);
                    printf("Enter the name of the subject: ");
                    scanf("%[^\n]%*c", sub->name);
                    printf("Enter percentage points midterm: ");
                    scanf("%d%*c", &sub->f1);
                    printf("Enter percentage points endterm: ");
                    scanf("%d%*c", &sub->f2);
                    printf("Enter the semester: ");
                    scanf("%[^\n]%*c", sub->semester);
                    printf("Enter the number of students: ");
                    scanf("%d%*c", &sub->studentCount);
                    sub->studentList = makeSllist();
                    saveFile(sub);
                    printf("Do you want to continue? (y/n): ");
                    scanf("%c%*c", &continue_flag);
                } while (continue_flag == 'y' || continue_flag == 'Y');
                break;
            case 2:
                do{
                    sub = (Subject *)malloc(sizeof(Subject));
                    sub->studentList = makeSllist();
                    printf("_____ Add score ______\n");
                    printf("Enter the id of the subject: ");
                    scanf("%[^\n]%*c", sub->id);
                    printf("Enter the semester: ");
                    scanf("%[^\n]%*c", sub->semester);
                    if(readData(sub) == -1) break;
                    elementtype *e = (elementtype*)malloc(sizeof(elementtype));
                    printf("Enter the id of student: ");
                    scanf("%[^\n]%*c", e->id);
                    printf("Enter the first name of student: ");
                    scanf("%[^\n]%*c", e->firstName);
                    printf("Enter the last name of student: ");
                    scanf("%[^\n]%*c", e->lastName);
                    printf("Enter the mid term point of student: ");
                    scanf("%f%*c", &e->midTermPoint);
                    printf("Enter the end of term point of student: ");
                    scanf("%f%*c", &e->endTermPoint);
                    strcpy(e->rank, getRank(e->midTermPoint, e->endTermPoint, sub->f1, sub->f2));
                    insertAtEndSllist(sub->studentList, *e);
                    sub->studentCount++;
                    quicksortSllist(sub->studentList);
                    saveFile(sub);
                    printf("Do you want to continue? (y/n): ");
                    scanf("%c%*c", &continue_flag);
                } while (continue_flag == 'y' || continue_flag == 'Y');
                break;
            case 3:
                 do{
                    sub = (Subject *)malloc(sizeof(Subject));
                    sub->studentList = makeSllist();
                    printf("_____ Remove Score ______\n");
                    printf("Enter the id of the subject: ");
                    scanf("%[^\n]%*c", sub->id);
                    printf("Enter the semester: ");
                    scanf("%[^\n]%*c", sub->semester);
                    if(readData(sub) == -1) break;
                    elementtype *e = (elementtype*)malloc(sizeof(elementtype));
                    printf("Enter the id of student: ");
                    scanf("%[^\n]%*c", e->id);
                    SllistNode *student = searchFunc(sub->studentList, *e);
                    if(student != NULL){
                        deleteNode(sub->studentList, student);
                        sub->studentCount--;
                        quicksortSllist(sub->studentList);
                        saveFile(sub);
                    }
                    printf("Do you want to continue? (y/n): ");
                    scanf("%c%*c", &continue_flag);
                } while (continue_flag == 'y' || continue_flag == 'Y');
                break;
            case 4:
                do{
                    sub = (Subject *)malloc(sizeof(Subject));
                    sub->studentList = makeSllist();
                    printf("_____ Search Score ______\n");
                    printf("Enter the id of the subject: ");
                    scanf("%[^\n]%*c", sub->id);
                    printf("Enter the semester: ");
                    scanf("%[^\n]%*c", sub->semester);
                    if(readData(sub) == -1) break;
                    elementtype *e = (elementtype*)malloc(sizeof(elementtype));
                    printf("Enter the id of student: ");
                    scanf("%[^\n]%*c", e->id);
                    SllistNode *student = searchFunc(sub->studentList, *e);
                    if(student != NULL){
                        printfSllistNode(student->element);
                    } else {
                        printf("--> Not found\n");
                    }
                    printf("Do you want to continue? (y/n): ");
                    scanf("%c%*c", &continue_flag);
                } while (continue_flag == 'y' || continue_flag == 'Y');
                break;
            case 5:
                do{
                    sub = (Subject *)malloc(sizeof(Subject));
                    sub->studentList = makeSllist();
                    printf("_____ Search Score ______\n");
                    printf("Enter the id of the subject: ");
                    scanf("%[^\n]%*c", sub->id);
                    printf("Enter the semester: ");
                    scanf("%[^\n]%*c", sub->semester);
                    if(readData(sub) == -1) break;
                    report(sub);
                    printf("Do you want to continue? (y/n): ");
                    scanf("%c%*c", &continue_flag);
                } while (continue_flag == 'y' || continue_flag == 'Y');
                break;
            default:
                return 0;
        }
    } while (ch!= 6);
}

void printfSllistNode(elementtype element){
    printf("S|%-8s|%-20s|%-10s| %-4.1f | %-4.1f | %-1s \n",
        element.id,
        element.firstName,
        element.lastName,
        element.midTermPoint,
        element.rank);
    return;
}


int func(elementtype e1, elementtype e2){
    return strcmp(e1.id, e2.id);
}

int sosanhSllist(SllistNode* student1,SllistNode* student2){
    return strcmp(student1->element.lastName, student2->element.lastName);
}

int readData(Subject* sub){
    char *fileName;
    fileName = (char *)malloc(sizeof(char)*20);
    fileName = strcpy(fileName, sub->id);
    strcat(fileName, "_");
    strcat(fileName, sub->semester);
    strcat(fileName, ".txt");
    FILE *fp = fopen(fileName, "r");
    char prefix[10];
    char data[100];
    int f_val;
    if(!fp){
        printf("Error. File %s khong ton tai!\n", fileName);
        return -1;
    }
    while(fscanf(fp,"%[^|]|%[^\n]\n", prefix, data) != NULL){
        //printf("%s|%s\n", prefix, data);
        switch(getPrefixVal(prefix)){
            case SUBJECT_ID:
                strcpy(sub->id, data);
                break;
            case SUBJECT:
                strcpy(sub->name, data);
                break;
            case F:
                if((f_val = atoi(strtok(data, "|")))){
                    sub->f1 = f_val;
                }
                if((f_val = atoi(strtok(NULL, "|")))){
                    sub->f2 = f_val;
                } else {
                    printf("Error. Dinh dang file chua dung!\n");
                    return -1;
                }
                break;
            case SEMESTER:
                strcpy(sub->semester, data);
                break;
            case STUDENT_COUNT:
                sub->studentCount = atoi(data);
                break;
            case S:
                if(sub->studentList == NULL){
                    sub->studentList = makeSllist();
                }

                elementtype *e = (elementtype*)malloc(sizeof(elementtype));
                strcpy(e->id, getColumnValue(data, 0));
                strcpy(e->firstName, getColumnValue(data, 1));
                strcpy(e->lastName, getColumnValue(data, 2));
                e->midTermPoint = atof(getColumnValue(data, 3));
                e->endTermPoint = atof(getColumnValue(data, 4));
                strcpy(e->rank, getColumnValue(data, 5));
                insertAtEndSllist(sub->studentList, *e);
                break;
            default:
                printf("Error. Dinh dang file sai!\n");
                return -1;
        }
        if(feof(fp)) {
            fclose(fp);
            return 0;
        }
    }
    return 0;
}

void saveFile(Subject *sub){
    char *fileName;
    fileName = (char *)malloc(sizeof(char)*20);
    fileName = strcpy(fileName, sub->id);
    strcat(fileName, "_");
    strcat(fileName, sub->semester);
    strcat(fileName, ".txt");
    FILE *fp = fopen(fileName, "w+");
    fprintf(fp, "SubjectID|%s\n", sub->id);
    fprintf(fp, "Subject|%s\n", sub->name);
    fprintf(fp, "F|%d|%d\n", sub->f1, sub->f2);
    fprintf(fp, "Semester|%s\n", sub->semester);
    fprintf(fp, "StudentCount|%d\n", sub->studentCount);
    SllistNode * Node;
    for(Node = sub->studentList->root; Node!=NULL; Node = Node->next)
    {
        fprintf(fp, "S|%s|%s|%s|%.1f|%.1f|%s\n",
                    Node->element.id,
                    Node->element.firstName,
                    Node->element.lastName,
                    Node->element.midTermPoint,
                    Node->element.endTermPoint,
                    Node->element.rank);
    }
    fclose(fp);
}

int getPrefixVal(char *key){
    if(strcmp(key, "SubjectID") == 0) return SUBJECT_ID;
    else if(strcmp(key, "Subject") == 0) return SUBJECT;
    else if(strcmp(key, "F") == 0) return F;
    else if(strcmp(key, "Semester") == 0) return SEMESTER;
    else if(strcmp(key, "StudentCount") == 0) return STUDENT_COUNT;
    else if(strcmp(key, "S") == 0) return S;
    else return -1;
}

char* getColumnValue(char *data, int index){
    char* result;
    result = (char*)malloc(sizeof(char)*20);
    char* copy_data = (char*)malloc(sizeof(char)*strlen(data));
    strcpy(copy_data, data);
    result = strtok(copy_data, "|");
    for(int i=0; i<index; i++){
        result = strtok(NULL, "|");
    }
    strtrim(result);
    return result;
}

void strtrim(char *s)
{
  int i, j;
  char st[500];
  i = j = 1;
  if (s[0] == ' ') j = 0;
  else  st[0] = s[0];
  while (s[i] != '\0')
    {
      if (s[i] == ' ' && s[i-1] == ' ')
	{
	  i++;
	  continue;
	}
      st[j] = s[i];
      j++;
      i++;
    }
  if (st[j-1] == ' ') j--;
  for (i=0; i<j; i++)
    {
      s[i] = st[i];
    }
  s[j] = '\0';
}


void printSubject(Subject *sub){
    printf("SubjectID|%s\n", sub->id);
    printf("Subject|%s\n", sub->name);
    printf("F|%d|%d\n", sub->f1, sub->f2);
    printf("Semester|%d\n", sub->semester);
    printf("StudentCount|%d\n", sub->studentCount);
    traverseSllist(sub->studentList);
}

char* getRank(float midTermPoint, float endTermPoint, int f1, int f2){
    float avgMark = getMark(midTermPoint, endTermPoint, f1, f2);
    if(avgMark >= 8.5) return "A";
    else if(avgMark >= 7) return "B";
    else if(avgMark >= 5.5) return "C";
    else if(avgMark >= 4) return "D";
    else return "F";
}

float getMark(float midTermPoint, float endTermPoint, int f1, int f2){
    return midTermPoint*(f1/100.0) + endTermPoint*(f2/100.0);
}

void report(Subject *sub){
    char *fileName;
    fileName = (char *)malloc(sizeof(char)*20);
    fileName = strcpy(fileName, sub->id);
    strcat(fileName, "_");
    strcat(fileName, sub->semester);
    strcat(fileName, "_rp.txt");
    FILE *fp = fopen(fileName, "w+");
    SllistNode * Node;
    float highestMark = 0;
    float lowestMark = 10;
    float mark = 0;
    float totalMark = 0;
    float avgMark = 0;
    int numRankA = getNumberOfRank(sub, "A");
    int numRankB = getNumberOfRank(sub, "B");
    int numRankC = getNumberOfRank(sub, "C");
    int numRankD = getNumberOfRank(sub, "D");
    int numRankF = getNumberOfRank(sub, "F");
    int i = 0;
    for(Node = sub->studentList->root; Node!=NULL; Node = Node->next)
    {
        mark = getMark(Node->element.midTermPoint, Node->element.endTermPoint, sub->f1, sub->f2);
        if(mark > highestMark) highestMark = mark;
    }
    for(Node = sub->studentList->root; Node!=NULL; Node = Node->next)
    {
        mark = getMark(Node->element.midTermPoint, Node->element.endTermPoint, sub->f1, sub->f2);
        if(mark < lowestMark) lowestMark = mark;
    }
    for(Node = sub->studentList->root; Node!=NULL; Node = Node->next)
    {
        mark = getMark(Node->element.midTermPoint, Node->element.endTermPoint, sub->f1, sub->f2);
        if(mark == highestMark){
            printf("The student with the highest mark is: %s %s\n", Node->element.firstName, Node->element.lastName);
            fprintf(fp, "The student with the highest mark is: %s %s\n", Node->element.firstName, Node->element.lastName);
        }
    }
    for(Node = sub->studentList->root; Node!=NULL; Node = Node->next)
    {
        mark = getMark(Node->element.midTermPoint, Node->element.endTermPoint, sub->f1, sub->f2);
        if(mark == lowestMark){
            printf("The student with the lowest mark is: %s %s\n", Node->element.firstName, Node->element.lastName);
            fprintf(fp, "The student with the lowest mark is: %s %s\n", Node->element.firstName, Node->element.lastName);
        }
    }
    for(Node = sub->studentList->root; Node!=NULL; Node = Node->next)
    {
        mark = getMark(Node->element.midTermPoint, Node->element.endTermPoint, sub->f1, sub->f2);
        totalMark += mark;
    }
    if(sub->studentCount != 0)
        avgMark = totalMark/sub->studentCount;
    else
        avgMark = 0;
    printf("The average mark is: %.2f\n", avgMark);
    fprintf(fp, "The average mark is: %.2f\n", avgMark);
    printf("A histogram of the subject %s is:\n", sub->id);
    fprintf(fp, "A histogram of the subject %s is:\n", sub->id);
    printf("A: "); fprintf(fp, "A: ");
    for(i = 0; i < numRankA; i++){
        printf("*"); fprintf(fp, "*");
    }
    printf("\n"); fprintf(fp,"\n");
    printf("B: "); fprintf(fp, "B: ");
    for(i = 0; i < numRankB; i++){
        printf("*"); fprintf(fp, "*");
    }
    printf("\n"); fprintf(fp,"\n");
    printf("C: "); fprintf(fp, "C: ");
    for(i = 0; i < numRankC; i++){
        printf("*"); fprintf(fp, "*");
    }
    printf("\n"); fprintf(fp,"\n");
    printf("D: "); fprintf(fp, "D: ");
    for(i = 0; i < numRankD; i++){
        printf("*"); fprintf(fp, "*");
    }
    printf("\n"); fprintf(fp,"\n");
    printf("F: "); fprintf(fp, "F: ");
    for(i = 0; i < numRankF; i++){
        printf("*"); fprintf(fp, "*");
    }
    printf("\n"); fprintf(fp,"\n");
    fclose(fp);
}

int getNumberOfRank(Subject *sub, const char* rank){
    int total = 0;
    SllistNode *Node;
    for(Node = sub->studentList->root; Node!=NULL; Node = Node->next)
    {
        if(strcmp(Node->element.rank, rank) == 0) total++;
    }
    return total;
}




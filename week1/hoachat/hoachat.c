#include <stdio.h>

enum Material{
    NONE_MATERIAL,
    JETS,
    FOG,
    FOAM,
    DRY_AGENT
};
enum Reactivity{
    NONE_REACTIVITY,
    V
};

enum Protection{
    NONE_PROTECTION,
    FULL,
    BA,
    BA_FOR_FIRE_ONLY,
};

enum Containment{
    NONE_CONTAINMENT,
    DILUTE,
    CONTAIN
};

enum Evacuation{
    NONE_EVACUATION,
    CONSIDER_EVACUATION,
};

typedef struct{
    enum Material material;
    enum Reactivity reactivity;
    enum Protection protection;
    enum Containment containment;
    enum Evacuation evacuation;
}EmergencyAction;

typedef struct{
    char key[2];
    EmergencyAction emergenAction;
} ElementType;

int checkHazchemLetter(char key_hazchem, ElementType* list, int n);
int checkHazchemCode(char *hazchem_code,  ElementType* list, int n);
EmergencyAction createEmergencyAction(enum Material material,
                                    enum Reactivity reactivity,
                                    enum Protection protection,
                                    enum Containment containment,
                                    enum Evacuation evacuation);

int main(){
    ElementType* listHazchem;
    char hazchemCode[4];
    int result;
    int i;
    char color_flag[4];
    int *hazchem_index_list;
    int index = 0;
    char flag_continue[4] = "";
    EmergencyAction *emeAct;
    listHazchem = (ElementType *)malloc(sizeof(ElementType)*18);
    strcpy(listHazchem[1].key, "1");
    listHazchem[1].emergenAction = createEmergencyAction(JETS, NONE_REACTIVITY, NONE_PROTECTION, NONE_CONTAINMENT, NONE_EVACUATION);
    strcpy(listHazchem[2].key, "2");
    listHazchem[2].emergenAction = createEmergencyAction(FOG, NONE_REACTIVITY, NONE_PROTECTION, NONE_CONTAINMENT, NONE_EVACUATION);
    strcpy(listHazchem[3].key, "3");
    listHazchem[3].emergenAction = createEmergencyAction(FOAM, NONE_REACTIVITY, NONE_PROTECTION, NONE_CONTAINMENT, NONE_EVACUATION);
    strcpy(listHazchem[4].key, "4");
    listHazchem[4].emergenAction = createEmergencyAction(DRY_AGENT, NONE_REACTIVITY, NONE_PROTECTION, NONE_CONTAINMENT, NONE_EVACUATION);
    strcpy(listHazchem[5].key, "P");
    listHazchem[5].emergenAction = createEmergencyAction(NONE_MATERIAL, V, FULL, DILUTE, NONE_EVACUATION);
    strcpy(listHazchem[6].key, "R");
    listHazchem[6].emergenAction = createEmergencyAction(NONE_MATERIAL, NONE_REACTIVITY, FULL, DILUTE, NONE_EVACUATION);
    strcpy(listHazchem[7].key, "S");
    listHazchem[7].emergenAction = createEmergencyAction(NONE_MATERIAL, V, BA, DILUTE, NONE_EVACUATION);
    strcpy(listHazchem[8].key, "S");
    listHazchem[8].emergenAction = createEmergencyAction(NONE_MATERIAL, V, BA_FOR_FIRE_ONLY, DILUTE, NONE_EVACUATION);
    strcpy(listHazchem[9].key, "T");
    listHazchem[9].emergenAction = createEmergencyAction(NONE_MATERIAL, NONE_REACTIVITY, BA, DILUTE, NONE_EVACUATION);
    strcpy(listHazchem[10].key, "T");
    listHazchem[10].emergenAction = createEmergencyAction(NONE_MATERIAL, NONE_REACTIVITY, BA_FOR_FIRE_ONLY, DILUTE, NONE_EVACUATION);
    strcpy(listHazchem[11].key, "W");
    listHazchem[11].emergenAction = createEmergencyAction(NONE_MATERIAL, V, FULL, CONTAIN, NONE_EVACUATION);
    strcpy(listHazchem[12].key, "X");
    listHazchem[12].emergenAction = createEmergencyAction(NONE_MATERIAL, NONE_REACTIVITY, FULL, CONTAIN, NONE_EVACUATION);
    strcpy(listHazchem[13].key, "Y");
    listHazchem[13].emergenAction = createEmergencyAction(NONE_MATERIAL, V, BA, CONTAIN, NONE_EVACUATION);
    strcpy(listHazchem[14].key, "Y");
    listHazchem[14].emergenAction = createEmergencyAction(NONE_MATERIAL, V, BA_FOR_FIRE_ONLY, CONTAIN, NONE_EVACUATION);
    strcpy(listHazchem[15].key, "Z");
    listHazchem[15].emergenAction = createEmergencyAction(NONE_MATERIAL, V, BA, CONTAIN, NONE_EVACUATION);
    strcpy(listHazchem[16].key, "Z");
    listHazchem[16].emergenAction = createEmergencyAction(NONE_MATERIAL, V, BA_FOR_FIRE_ONLY, CONTAIN, NONE_EVACUATION);
    strcpy(listHazchem[17].key, "E");
    listHazchem[17].emergenAction = createEmergencyAction(NONE_MATERIAL, NONE_REACTIVITY, NONE_PROTECTION, NONE_CONTAINMENT, CONSIDER_EVACUATION);

    do{
        printf("____ HAZCHEM CODE PROGRAM ____\n");
        printf("Enter HAZCHEM Code: ");
        scanf("%[^\n]%*c", hazchemCode);
        while(strlen(hazchemCode) > 3 || checkHazchemCode(hazchemCode, listHazchem, 17) == -1)
        {
            printf("Error. HAZCHEM Code do not exits!\n");
            printf("Enter HAZCHEM Code: ");
            scanf("%[^\n]%*c", hazchemCode);
        }
        hazchem_index_list = (int *)malloc(sizeof(int)*strlen(hazchemCode));
        for(i = 0; i < strlen(hazchemCode); i++)
        {
            result = checkHazchemLetter(hazchemCode[i], listHazchem, 17);
            if(result == 7 ||
            result == 9 ||
            result == 13 ||
            result == 15)
            {
                printf("Is the S reverse coloured?");
                scanf("%[^\n]%*c", &color_flag);
                if(strcmp(color_flag, "yes") == 0)
                {
                    hazchem_index_list[index++] = result + 1;
                }
                else
                {
                    hazchem_index_list[index++] = result;
                }
            }
            else
            {
                hazchem_index_list[index++] = result;
            }
        }
        for(i = 0; i < index; i++)
        {
            printEmergencyAction(listHazchem[hazchem_index_list[i]]);
        }
        printf("Do you want to continue(yes/no): ");
        scanf("%[^\n]%*c", flag_continue);
    } while (strcmp(flag_continue, "yes") == 0);
}

int checkHazchemLetter(char key_hazchem, ElementType* list, int n){
    int i;
    char key[2];
    key[0] = key_hazchem;
    key[1] = '\0';
    for(i=1; i<=n; i++){
        if(strcmp(key, list[i].key) == 0) return i;
    }
    return -1;
}

int checkHazchemCode(char *hazchem_code, ElementType* list, int n){
    int i;
    for(i = 0; i < strlen(hazchem_code); i++){
        if(checkHazchemLetter(hazchem_code[i], list, n) == -1) return -1;
    }
    return 1;
}

EmergencyAction createEmergencyAction(enum Material material,
                                    enum Reactivity reactivity,
                                    enum Protection protection,
                                    enum Containment containment,
                                    enum Evacuation evacuation){
    EmergencyAction emeAct;
    emeAct.material = material;
    emeAct.reactivity = reactivity;
    emeAct.protection = protection;
    emeAct.containment = containment;
    emeAct.evacuation = evacuation;
    return emeAct;
}

void printEmergencyAction(ElementType e){
    switch (e.emergenAction.material){
        case NONE_MATERIAL:
            break;
        case JETS:
            printf("Material: Jets\n");
            break;
        case FOG:
            printf("Material: Fog\n");
            break;
        case FOAM:
            printf("Material: Foam\n");
            break;
        case DRY_AGENT:
            printf("Material: Dry agent\n");
            break;
    }

    switch (e.emergenAction.reactivity){
        case NONE_REACTIVITY:
            break;
        case V:
            printf("Reactivity: có thể phản ứng mạnh\n");
            break;
    }

    switch (e.emergenAction.protection){
        case NONE_PROTECTION:
            break;
        case FULL:
            printf("Protection: Bảo vệ toàn thân với quần áo và mặt nạ\n");
            break;
        case BA:
            printf("Protection: Cần mặt nạ phòng độc và găng tay bảo vệ\n");
            break;
        case BA_FOR_FIRE_ONLY:
            printf("Protection: Cần mặt nạ phòng độc và găng tay bảo vệ chi khi co chay\n");
            break;
    }

    switch (e.emergenAction.containment){
        case NONE_CONTAINMENT:
            break;
        case DILUTE:
            printf("Containment: hóa chất có thể được pha loãng với nước và rửa sạch\n");
            break;
        case CONTAIN:
            printf("Containment: hóa chất phải được chứa trong các vật chứa. Không cho tiếp xúc với hóa chất khác và nước.\n");
            break;
    }

    switch (e.emergenAction.evacuation){
        case NONE_EVACUATION:
            break;
        case CONSIDER_EVACUATION:
            printf("Evacuation: consider evacuation\n");
            break;
    }
}




